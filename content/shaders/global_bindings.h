#ifndef GLOBAL_BINDINGS_H
#define GLOBAL_BINDINGS_H

// Buffer bindings
#define VOXEL_CHUNK_DATA 0
#define VOXEL_COLOR_DATA 1
#define VOXEL_CHUNK_INDICES 2

// Sampler bindings
#define BINDING_GLOBAL_REPEAT_SAMPLER 0
#define BINDING_GLOBAL_CLAMP_SAMPLER 1

#endif