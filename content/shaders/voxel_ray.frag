#version 450
#extension GL_EXT_nonuniform_qualifier: require
#extension GL_ARB_shader_draw_parameters: require

#include "common.h"

layout(location = 0) in vec2 texCoords;

layout(location = 0) out vec4 outColor;

layout(push_constant) uniform viewport_data
{
    float x;
    float y;
    float width;
    float height;
    float near;
    float far;
} viewport;

layout(set = 0, binding = 4) uniform view_info_t {
    mat4 view;
    mat4 proj;
    vec3 pos;
} view_info;

layout(set = 0, binding = 0, std430) readonly buffer SSBOs
{
    uint data[];
} ssbos[];

layout(set = 0, binding = 0, std430) readonly buffer SSBO2
{
    vec3 data[];
} ssbo2[];

#define voxel_chunk_data ssbos[VOXEL_CHUNK_DATA].data
#define voxel_chunk_indices ssbos[VOXEL_CHUNK_INDICES].data
#define voxel_color_data ssbo2[VOXEL_COLOR_DATA].data

#define EMPTY_CHUNK_INDEX -1

const uint MAXIMUM_TRACE_DISTANCE = 500;

#define CHUNK_WIDTH 8
const ivec3 VOXEL_CHUNK_SIZE = ivec3(CHUNK_WIDTH);

#define WORLD_WIDTH_IN_CHUNKS 48
const ivec3 WORLD_SIZE_IN_CHUNKS = ivec3(WORLD_WIDTH_IN_CHUNKS);

vec3 transform_to_voxel_space(vec3 pos)
{
    return pos + WORLD_SIZE_IN_CHUNKS * VOXEL_CHUNK_SIZE / 2 - vec3(1.0);
}

uint get_3d_index(ivec3 p, ivec3 s)
{
    return p.x + s.x * p.y + s.x * s.y * p.z;
}

void get_hit_and_normal(vec3 ray_start, vec3 ray_dir, ivec3 ray_step, ivec3 voxel_pos, out vec3 hit_pos, out vec3 normal)
{
    const vec3 inv_ray_dir = 1 / ray_dir * ivec3(notEqual(ray_dir, vec3(0.0))) + 1e9f * ivec3(equal(ray_dir, vec3(0.0)));
    const vec3 t0 = (voxel_pos - ray_start) * inv_ray_dir;
    const vec3 t1 = (voxel_pos + 1 - ray_start) * inv_ray_dir;
    
    const vec3 t_min = min(t0, t1);

    const int b_max_x = int(t_min.x >= t_min.y && t_min.x >= t_min.z);
    const int b_max_y = int(t_min.y >= t_min.z && t_min.y >= t_min.x);
    const int b_max_z = int(t_min.z >= t_min.x && t_min.z >= t_min.y);
    
    normal = -ray_step * ivec3(b_max_x, b_max_y, b_max_z);
    
    float time_min = max(max(t_min.x, t_min.y), t_min.z);
    
    if (time_min < 0.0)
    {
        normal = -ray_dir;
    }
    time_min = max(time_min, 0);
    
    hit_pos = ray_start + ray_dir * t_min;
}

// simple lighting calculation with single directional light
vec3 compute_lighting(vec3 voxel_pos, vec3 ray_start, vec3 ray_dir, ivec3 step)
{
    vec3 hit_pos = vec3(0.0);
    vec3 normal = vec3(0.0);

    get_hit_and_normal(ray_start, ray_dir, step, ivec3(voxel_pos), hit_pos, normal);
    vec3 light_position = transform_to_voxel_space(vec3(10.0, 128.0, -128.0));
    vec3 direction_of_light = normalize(light_position - hit_pos);

    float diffuse_intensity = max(0.0, dot(normal, direction_of_light));

    ivec3 chunk_pos = ivec3(voxel_pos / VOXEL_CHUNK_SIZE);
    uint chunk_id = get_3d_index(chunk_pos, WORLD_SIZE_IN_CHUNKS);
    
    uint chunk_index = uint(voxel_chunk_indices[chunk_id]);
    
    uint voxel_index = get_3d_index(ivec3(mod(voxel_pos, VOXEL_CHUNK_SIZE)), VOXEL_CHUNK_SIZE);
    
    vec3 voxel_color = voxel_color_data[chunk_index * CUBE(CHUNK_WIDTH) + voxel_index];
    
    const float spec_strength = 0.5;
    vec3 reflect_dir = reflect(-direction_of_light, normal);
    float spec_intensity = pow(max(dot(ray_dir, reflect_dir), 0.0), 32) * spec_strength;
    return voxel_color * (diffuse_intensity + spec_intensity);
}

bool chunk_intersects(ivec3 chunk_pos, out int chunk_index)
{
    if (any(greaterThanEqual(chunk_pos, WORLD_SIZE_IN_CHUNKS)) || any(lessThan(chunk_pos, ivec3(0)))) return false;

    uint chunk_id = get_3d_index(chunk_pos, WORLD_SIZE_IN_CHUNKS);
    chunk_index = int(voxel_chunk_indices[chunk_id]);
    return chunk_index != EMPTY_CHUNK_INDEX && chunk_index >= 0;
}

ivec3 get_voxel_local_pos(vec3 pos)
{
    return ivec3(mod(floor(pos), VOXEL_CHUNK_SIZE));
}

/** check if a local chunk position contains a voxel */
bool voxel_intersects(ivec3 local_pos, int chunk_index)
{
    if (any(greaterThanEqual(local_pos, VOXEL_CHUNK_SIZE))) return false;

    uint voxel_3d_index = get_3d_index(local_pos, VOXEL_CHUNK_SIZE);
    uint voxel_word_index = voxel_3d_index / 32;
    uint voxel_bit_index = voxel_3d_index % 32;

    return TEST_BIT(voxel_chunk_data[chunk_index * CUBE(CHUNK_WIDTH) / 32 + voxel_word_index], voxel_bit_index);
}

bool check_intersects(vec3 pos)
{
    int chunk_index;
    if (chunk_intersects(ivec3(pos / VOXEL_CHUNK_SIZE), chunk_index))
    {
        ivec3 local_pos = get_voxel_local_pos(pos);
        return voxel_intersects(local_pos, chunk_index);
    }
    return false;
}

bool voxel_traversal(vec3 ray_start, vec3 ray_dir, out vec3 intersection_pos)
{
    ivec3 current_voxel_pos = ivec3(floor(ray_start));

    const vec3 ray_end = ray_start + ray_dir * VOXEL_CHUNK_SIZE;
    const ivec3 last_voxel_pos = ivec3(floor(ray_end));

    // Adding sign(ray_dir) == 0 will prevent the step from ever being 0
    const ivec3 step = ivec3(sign(ray_dir)) + ivec3(equal(sign(ray_dir), vec3(0.0)));
    const vec3 next_voxel_bounds = (current_voxel_pos + step);
    vec3 tmax = (next_voxel_bounds - ray_start) / ray_dir;
    vec3 tdelta = 1 / ray_dir * step;

    ivec3 diff = ivec3(0);
    bool neg_ray = false;
    if (current_voxel_pos.x != last_voxel_pos.x && ray_dir.x < 0) { diff.x--; neg_ray=true; }
    if (current_voxel_pos.y != last_voxel_pos.y && ray_dir.y < 0) { diff.y--; neg_ray=true; }
    if (current_voxel_pos.z != last_voxel_pos.z && ray_dir.z < 0) { diff.z--; neg_ray=true; }

    if (check_intersects(current_voxel_pos))
    {
        intersection_pos = current_voxel_pos;
        return true;
    }

    if (neg_ray)
    {
        current_voxel_pos += diff;
        if (check_intersects(current_voxel_pos))
        {
            intersection_pos = current_voxel_pos;
            return true;
        }
    }

    while (last_voxel_pos != current_voxel_pos)
    {
        if (tmax.x < tmax.y)
        {
            if (tmax.x < tmax.z)
            {
                current_voxel_pos.x += step.x;
                tmax.x += tdelta.x;
            }
            else
            {
                current_voxel_pos.z += step.z;
                tmax.z += tdelta.z;
            }
        }
        else
        {
            if (tmax.y < tmax.z)
            {
                current_voxel_pos.y += step.y;
                tmax.y += tdelta.y;
            }
            else
            {
                current_voxel_pos.z += step.z;
                tmax.z += tdelta.z;
            }
        }

        if (check_intersects(current_voxel_pos))
        {
            intersection_pos = current_voxel_pos;
            return true;
        }
    }
    return false;
}

vec3 chunk_traversal(vec3 ray_start, vec3 ray_dir)
{
    ivec3 current_chunk_pos = ivec3(floor(ray_start) / VOXEL_CHUNK_SIZE);

    const vec3 ray_end = ray_start + ray_dir * MAXIMUM_TRACE_DISTANCE;
    const ivec3 last_chunk_pos = ivec3(floor(ray_end) / VOXEL_CHUNK_SIZE);

    // Adding sign(ray_dir) == 0 will prevent the step from ever being 0
    const ivec3 step = ivec3(sign(ray_dir)) + ivec3(equal(sign(ray_dir), vec3(0.0)));
    const vec3 next_chunk_bounds = (current_chunk_pos + step) * VOXEL_CHUNK_SIZE;
    vec3 tmax = (next_chunk_bounds - ray_start) / ray_dir;
    vec3 tdelta = VOXEL_CHUNK_SIZE / ray_dir * step;

    ivec3 diff = ivec3(0);
    bool neg_ray = false;
    if (current_chunk_pos.x != last_chunk_pos.x && ray_dir.x < 0) { diff.x--; neg_ray=true; }
    if (current_chunk_pos.y != last_chunk_pos.y && ray_dir.y < 0) { diff.y--; neg_ray=true; }
    if (current_chunk_pos.z != last_chunk_pos.z && ray_dir.z < 0) { diff.z--; neg_ray=true; }

    int chunk_index;
    vec3 intersection_pos;
    if (chunk_intersects(current_chunk_pos, chunk_index))
    {
        if (voxel_traversal(current_chunk_pos * VOXEL_CHUNK_SIZE, ray_dir, intersection_pos))
        {
            return compute_lighting(intersection_pos, ray_start, ray_dir, step);
        }
    }

    if (neg_ray)
    {
        current_chunk_pos += diff;
        if (chunk_intersects(current_chunk_pos, chunk_index))
        {
            if (voxel_traversal(current_chunk_pos * VOXEL_CHUNK_SIZE, ray_dir, intersection_pos))
            {
                return compute_lighting(intersection_pos, ray_start, ray_dir, step);
            }
        }
    }

    int num_steps = 0;
    while (last_chunk_pos != current_chunk_pos && num_steps < MAXIMUM_TRACE_DISTANCE)
    {
        if (tmax.x < tmax.y)
        {
            if (tmax.x < tmax.z)
            {
                current_chunk_pos.x += step.x;
                tmax.x += tdelta.x;
            }
            else
            {
                current_chunk_pos.z += step.z;
                tmax.z += tdelta.z;
            }
        }
        else
        {
            if (tmax.y < tmax.z)
            {
                current_chunk_pos.y += step.y;
                tmax.y += tdelta.y;
            }
            else
            {
                current_chunk_pos.z += step.z;
                tmax.z += tdelta.z;
            }
        }

        if (chunk_intersects(current_chunk_pos, chunk_index))
        {
            if (voxel_traversal(current_chunk_pos * VOXEL_CHUNK_SIZE, ray_dir, intersection_pos))
            {
                return compute_lighting(intersection_pos, ray_start, ray_dir, step);
            }
        }

        ++num_steps;
    }
    discard;
}

void main()
{
    vec4 ndcPos;
    ndcPos.xy = ((2.0 * gl_FragCoord.xy) - (2.0 * vec2(viewport.x, viewport.y))) / (vec2(viewport.width, viewport.height)) - 1;
    ndcPos.z = (2.0 * gl_FragCoord.z - viewport.near - viewport.far) / (viewport.far - viewport.near);
    ndcPos.w = 1.0;

    vec3 camera_position = view_info.pos;
    vec3 ro = transform_to_voxel_space(camera_position);

    vec3 direction = vec4(inverse(view_info.proj) * vec4(ndcPos.xy, 0.0, 1.0)).xyz;
    direction = vec4(view_info.view * vec4(direction, 0.0)).xyz;
    direction = normalize(direction);

    vec3 rd = direction;

    vec3 color = chunk_traversal(ro, rd);
    outColor = vec4(color, 1.0);
    gl_FragDepth = 0.5; // #todo [roey]: Implement proper frag depth ouput
}