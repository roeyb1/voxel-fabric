#ifndef COMMON_H
#define COMMON_H

#include "global_bindings.h"

#define PI 3.14159265359
#define FLT_MAX 3.402823466e+38

#define TEST_BIT(number, pos) ((number & (1 << pos)) != 0)
#define CUBE(x) (x * x * x)

#endif