#version 450
#extension GL_EXT_nonuniform_qualifier: require
#extension GL_ARB_shader_draw_parameters: require

layout(location = 0) out vec2 texCoords;

void main()
{
    const vec2 vertices[3] = vec2[3](vec2(-1, -1), vec2(3, -1), vec2(-1, 3));
    gl_Position = vec4(vertices[gl_VertexIndex], 0, 1);
    texCoords = 0.5 * gl_Position.xy + vec2(0.5);
}