echo off
echo Compiling Shaders...
glslc.exe -g fullscreen_quad.vert -o fullscreen_quad.spv 
glslc.exe -g voxel_ray.frag -o voxel_frag.spv

if ERRORLEVEL 1 pause

copy fullscreen_quad.spv ..\..\bin\Debug\content\shaders
copy voxel_frag.spv ..\..\bin\Debug\content\shaders

copy fullscreen_quad.spv ..\..\bin\Profile\content\shaders
copy voxel_frag.spv ..\..\bin\Profile\content\shaders

copy fullscreen_quad.spv ..\..\bin\Release\content\shaders
copy voxel_frag.spv ..\..\bin\Release\content\shaders