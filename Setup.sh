#!/bin/sh
pushd scripts
python3 Setup.py
popd

echo Copying precommit hook to .git directory...
cp scripts/pre-commit .git/hooks

pushd content/shaders
./compile_shaders.sh
popd
