# Fabric Engine

## Multithreading focused 2D and 3D game engine

Fabric uses a fiber based job system at the core of the engine. It allows the engine to be highly parallel with a low cost context switching system that prevents worker threads from locking down.

The job scheduler is heavily inspired by [Fiber based job system](https://ourmachinery.com/post/fiber-based-job-system/) and ["Parallelizing the Naughty Dog Engine using fibers"](https://www.gdcvault.com/play/1022186/Parallelizing-the-Naughty-Dog-Engine).

Fabric also features a full PBR pipeline using the Vulkan api and fully bindless descriptor sets!

![image](/uploads/2f69fb35802211bdea234643773f937a/image.png)

## Design Philosophy

The main goal with Fabric is to avoid as much complexity in the code as possible. Keeping code simple and verbose makes it easier for developers to grasp the main concepts. It's the main reason why the majority of the engine is written in C. C is just a simpler language than C++ and allows the code to be significantly more clear and simple.

Some examples:
- If the theoretical maximum size of an array can be determined beforehand, use a static buffer instead of a vector.


## Getting started

### Project structure

Source code directories:

- `core/` contains the main core static library which the main executable is linked against
- `modules/` contain all additional functionality. Each subdirectory represents a shared library which can be hot-loaded from the engine.
- `vendor/` contains all the third party code that our modules might depend on
- `unit_test/` contains the unit test executable

Build system:

- `scripts/` contains the python scripts which set up the build environment.
- `premake5.lua` defines all the build configurations and targets.
- `Setup.bat` initializes the build environment and creates the IDE project file.

When the project is built, all intermediate object files and project files are stored in the `build/` directory.
The final binary is output to `bin/{configuration}/` where configuration could be Debug/Release/Profile.
