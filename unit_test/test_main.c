#include <stdlib.h>

#include <core/atomic.h>
#include <core/core_types.h>
#include <core/hash.inl>
#include <core/job_scheduler.h>
#include <core/log.h>
#include <core/os.h>

#pragma region Hash Tests

static bool run_hash_tests()
{
    typedef struct test_struct
    {
        uint64_t val;
    } test_struct;

    typedef struct FA_HASH_T(uint64_t, test_struct) fa_hash_test_t;

    fa_hash_test_t hashmap = { 0 };
    for (uint64_t i = 0; i < 100; ++i)
    {
        test_struct v = { .val = i };
        fa_hash_add(&hashmap, i, v);
    }

    bool valid = true;
    for (uint64_t i = 0; i < 100; ++i)
    {
        const test_struct* val = fa_hash_get(&hashmap, i);
        if (!val)
        {
            valid = false;
            break;
        }

        if (val->val != i)
        {
            valid = false;
            break;
        }
    }

    valid &= fa_hash_remove(&hashmap, 5);
    valid &= (fa_hash_get(&hashmap, 5) == NULL);
    valid &= !fa_hash_remove(&hashmap, 5);

    fa_hash_add(&hashmap, 5, (test_struct){ .val = 0 });
    valid &= fa_hash_get(&hashmap, 5) != NULL;

    fa_hash_free(&hashmap);
    return valid;
}

#pragma endregion

#pragma region Job Tests

static void job_test_func(void* data)
{
    atomic_uint_least32_t* data_ptr = (atomic_uint_least32_t*)data;
    atomic_fetch_sub_uint32_t(data_ptr, 1);
}

static void job_spawning_jobs(void* data)
{
    fa_job_decl_t job_decls[100];
    for (uint32_t i = 0; i < 100; ++i)
    {
        job_decls[i].func = job_test_func;
        job_decls[i].user_data = data;
    }
    fa_atomic_count_t* count = fa_get_job_scheduler_api()->post_jobs(job_decls, 100);
    fa_get_job_scheduler_api()->wait_on_count_and_free(count);
}

static bool run_job_tests()
{
    const struct fa_job_scheduler_api* job_api = fa_init_job_scheduler(fa_os_api->thread, 8, 128, 128 * 1024);

    atomic_uint_least32_t test_counter = 100 * 10;
    fa_job_decl_t job_decls[10];
    for (uint32_t i = 0; i < 10; ++i)
    {
        job_decls[i].func = job_spawning_jobs;
        job_decls[i].user_data = &test_counter;
    }

    fa_atomic_count_t* count = job_api->post_jobs(job_decls, 10);
    job_api->wait_on_count_no_fiber(count);

    return test_counter == 0;
}

#pragma endregion

static int run_unit_tests()
{
    bool success = true;

    const bool hash_result = run_hash_tests();
    success &= hash_result;
    if (!hash_result)
    {
        FA_LOG(Error, "Failed hash tests!");
    }

    const bool job_result = run_job_tests();
    success &= job_result;
    if (!job_result)
    {
        FA_LOG(Error, "Failed job tests!");
    }

    return success ? EXIT_SUCCESS : EXIT_FAILURE;
}

int main(int argc, char** argv)
{
    const int result = run_unit_tests();
    if (result == EXIT_SUCCESS)
    {
        FA_LOG(Info, "All tests passed");
    }

    return result;
}