-- Make sure incluedirs() also calls sysincludedirs()
builtin_includedirs = includedirs
function includedirs(dirs)
    builtin_includedirs(dirs)
    sysincludedirs(dirs)
end

function link_to_vulkan()
	filter "platforms:Win64"
		links {"$(VULKAN_SDK)/lib/vulkan-1"}
		includedirs{"$(VULKAN_SDK)/include"}
	filter "platforms:MacOSX or Linux"
		links {"vulkan"}
	filter{}
end

function folder(t)
    if type(t) ~= "table" then t = {t} end
    for _,f in ipairs(t) do
        files {f .. "/**.h",  f .. "/**.c", f .. "/**.inl", f .. "/**.cpp" }
    end
end

-- basic fabric project setup
function fabric_project(name)
    project(name)
        language "C++"
		location("build/" .. name)
		targetdir "bin/%{cfg.buildcfg}"
		-- temporarily allow including vulkan everywhere since we are reusing the vulkan structs
        includedirs { "", "vendor/cglm/include", "$(VULKAN_SDK)/include" }
		folder { name }
		defines {"FA_LINKS_TO_" .. string.upper(name)}
		-- link to the profiler dll
		dependson {"tracy"}
		includedirs{"vendor/tracy"}
		defines{"TRACY_IMPORTS"}
		links{"tracy"}
end

-- specialize project for modules
function fabric_module(name)
    fabric_project(name)
        location("build/modules/" .. name)
        kind "SharedLib"
        targetdir "bin/%{cfg.buildcfg}/modules"
        targetname("fa_" .. name)
        dependson("core")
        folder {"modules/" .. name}
		includedirs{"vendor/"}
end

function main_exe(name)
	fabric_project(name)
        location("build/" .. name .. "_exe")
        targetname(name)
        kind "ConsoleApp"
        language "C++"
        targetdir "bin/%{cfg.buildcfg}"
        dependson {"core"}
		defines {"FA_LINKS_TO_CORE"}
        links {"core"}
end
		
workspace "VoxelFabric"
    configurations {"Debug", "Release", "Profile"}
    language "C++"
    cppdialect "C++17"
    flags 
	{
		"FatalWarnings",
		"MultiProcessorCompile"
	}
    warnings "Extra"
    inlining "Auto"
    editandcontinue "Off"
	exceptionhandling ("off")
	rtti("off")
    -- Enable this to test compile with strict aliasing.
    -- strictaliasing "Level3"
    -- Enable this to report build times, see: http://aras-p.info/blog/2019/01/21/Another-cool-MSVC-flag-d1reportTime/
    -- buildoptions { "/Bt+", "/d2cgsummary", "/d1reportTime" }
	debugdir "bin/%{cfg.buildcfg}"
	startproject "fabric"

	
filter { "system:windows" }
    platforms { "Win64" }

filter { "system:macosx" }
    platforms { "MacOSX" }

filter {"system:linux"}
    platforms { "Linux" }
	
-- Windows platform settings
filter { "platforms:Win64" }
    defines { "FA_OS_WINDOWS", "_CRT_SECURE_NO_WARNINGS" }
    staticruntime "On"
    architecture "x64"
    buildoptions 
	{
        "/utf-8",	-- Source encoding is UTF-8.
    }
    disablewarnings 
	{
        "4100", -- Unused formal parameter.
		"4152", -- Conversion from function pointer to void *.
		"4201", -- Nameless struct/union.
		"4706", -- assignment within conditional
		"4206", -- translation unit is empty. May be #ifdef'd out
    }
    defines {'_ITERATOR_DEBUG_LEVEL=0'}

-- MacOS platform settings
filter { "platforms:MacOSX" }
    defines { "FA_OS_MACOSX", "FA_OS_POSIX" }
    architecture "x64"
    buildoptions
	{
        "-fms-extensions",
        "-mavx",
        "-mfma",
    }
    enablewarnings 
	{
		"shadow",
		--"padded", todo
    }
    disablewarnings
	{
		"unused-parameter",
		"unused-function",
		"missing-field-initializers",
		"missing-braces",
    }

filter {"platforms:Linux"}
    defines { "FA_OS_LINUX", "FA_OS_POSIX" }
    architecture "x64"
    toolset "clang"
    buildoptions
	{
    }
    enablewarnings
	{
    }
    disablewarnings
	{
		"unused-parameter",
		"unused-function",
		"missing-field-initializers",
		"missing-braces",
    }

filter "configurations:Debug"
    defines { "FA_DEBUG", "DEBUG", "FA_PROFILE", "TRACY_ENABLE" }
    symbols "On"
	optimize "On"

filter "configurations:Release"
    defines { "FA_RELEASE", "NDEBUG" }
    optimize "On"

filter "configurations:Profile"
	defines { "FA_RELEASE", "NDEBUG", "FA_PROFILE", "TRACY_ENABLE"}
	symbols "On"
	optimize "On"
filter {}

group "vendor"

	fabric_project "glfw"
		location "build/vendor/glfw"
		targetdir "bin/%{cfg.buildcfg}" -- we need glfw dll in the root next to the exe
		language "C++"
		kind "SharedLib"
		defines{"_GLFW_BUILD_DLL"}
		files
		{
			"vendor/glfw/include/GLFW/glfw3.h",
			"vendor/glfw/include/GLFW/glfw3native.h",
			"vendor/glfw/src/context.c",
			"vendor/glfw/src/init.c",
			"vendor/glfw/src/input.c",
			"vendor/glfw/src/monitor.c",
			"vendor/glfw/src/vulkan.c",
			"vendor/glfw/src/window.c"
		}
		filter "system:windows"
			files
			{
				"vendor/glfw/src/win32_init.c",
				"vendor/glfw/src/win32_joystick.c",
				"vendor/glfw/src/win32_monitor.c",
				"vendor/glfw/src/win32_time.c",
				"vendor/glfw/src/win32_thread.c",
				"vendor/glfw/src/win32_window.c",
				"vendor/glfw/src/wgl_context.c",
				"vendor/glfw/src/egl_context.c",
				"vendor/glfw/src/osmesa_context.c"
			}
			defines 
			{ 
				"_GLFW_WIN32",
				"_CRT_SECURE_NO_WARNINGS"
			}
			disablewarnings
			{
				"4244", -- 'conversion' conversion from 'type1' to 'type2', possible loss of data.
			}

		filter "system:macosx"
			links
			{
				"Cocoa.framework",
				"Carbon.framework",
				"CoreVideo.framework",
				"IOKit.framework",
			}
			files
			{
				"vendor/glfw/src/cocoa_init.m",
				"vendor/glfw/src/cocoa_joystick.m",
				"vendor/glfw/src/cocoa_monitor.m",
				"vendor/glfw/src/cocoa_time.c",
				"vendor/glfw/src/cocoa_window.m",
				"vendor/glfw/src/egl_context.c",
				"vendor/glfw/src/nsgl_context.m",
				"vendor/glfw/src/osmesa_context.c",
				"vendor/glfw/src/posix_thread.c",
			}
			defines
			{
				"_GLFW_COCOA"
			}
	
		filter "system:linux"
			pic "On"
			files
			{
				"vendor/glfw/src/x11_init.c",
				"vendor/glfw/src/x11_monitor.c",
				"vendor/glfw/src/x11_window.c",
				"vendor/glfw/src/xkb_unicode.c",
				"vendor/glfw/src/posix_time.c",
				"vendor/glfw/src/posix_thread.c",
				"vendor/glfw/src/glx_context.c",
				"vendor/glfw/src/egl_context.c",
				"vendor/glfw/src/osmesa_context.c",
				"vendor/glfw/src/linux_joystick.c"
			}
			defines
			{
				"_GLFW_X11"
			}
			disablewarnings
			{
				"sign-compare"
			}
		filter {}
			
	 project "tracy"
        language "C++"
        location("build/vendor/tracy")
        kind "SharedLib"
        targetdir "bin/%{cfg.buildcfg}"
        targetname("tracy")
		defines {"TRACY_EXPORTS"}
        files
		{
			"vendor/tracy/TracyClient.cpp",
			"vendor/tracy/TracyC.h",
		}
		
		filter "system:windows"
			disablewarnings
			{
				"4244", -- 'conversion' conversion from 'type1' to 'type2', possible loss of data.
				"4505", -- unreferenced function with internal linkage has been removed
			}
			
		filter "system:macosx or linux"
			disablewarnings
			{
				"shadow",
			}
		
		filter "system:linux"
			linkoptions {"-ldl", "-pthread" }
		filter {}
			
	project "freetype2"
		language "C++"
		kind "SharedLib"
		targetname "freetype2"
        location("build/vendor/freetype2")
		includedirs {"vendor/freetype2/include", "vendor/freetype2/src" }
        targetdir "bin/%{cfg.buildcfg}"
		defines {"FT2_BUILD_LIBRARY", "DLL_EXPORT"}
		files
		{
			"vendor/freetype2/src/autofit/autofit.c",
			"vendor/freetype2/src/base/ftbase.c",
			"vendor/freetype2/src/base/ftbbox.c",
			"vendor/freetype2/src/base/ftbdf.c",
			"vendor/freetype2/src/base/ftbitmap.c",
			"vendor/freetype2/src/base/ftcid.c",
			"vendor/freetype2/src/base/ftfstype.c",
			"vendor/freetype2/src/base/ftgasp.c",
			"vendor/freetype2/src/base/ftglyph.c",
			"vendor/freetype2/src/base/ftgxval.c",
			"vendor/freetype2/src/base/ftinit.c",
			"vendor/freetype2/src/base/ftmm.c",
			"vendor/freetype2/src/base/ftotval.c",
			"vendor/freetype2/src/base/ftpatent.c",
			"vendor/freetype2/src/base/ftpfr.c",
			"vendor/freetype2/src/base/ftstroke.c",
			"vendor/freetype2/src/base/ftsynth.c",
			"vendor/freetype2/src/base/ftsystem.c",
			"vendor/freetype2/src/base/fttype1.c",
			"vendor/freetype2/src/base/ftwinfnt.c",
			"vendor/freetype2/src/bdf/bdf.c",
			"vendor/freetype2/src/bzip2/ftbzip2.c",
			"vendor/freetype2/src/cache/ftcache.c",
			"vendor/freetype2/src/cff/cff.c",
			"vendor/freetype2/src/cid/type1cid.c",
			"vendor/freetype2/src/gzip/ftgzip.c",
			"vendor/freetype2/src/lzw/ftlzw.c",
			"vendor/freetype2/src/pcf/pcf.c",
			"vendor/freetype2/src/pfr/pfr.c",
			"vendor/freetype2/src/psaux/psaux.c",
			"vendor/freetype2/src/pshinter/pshinter.c",
			"vendor/freetype2/src/psnames/psnames.c",
			"vendor/freetype2/src/raster/raster.c",
			"vendor/freetype2/src/sfnt/sfnt.c",
			"vendor/freetype2/src/smooth/smooth.c",
			"vendor/freetype2/src/truetype/truetype.c",
			"vendor/freetype2/src/type1/type1.c",
			"vendor/freetype2/src/type42/type42.c",
			"vendor/freetype2/src/winfonts/winfnt.c",
			"vendor/freetype2/src/ftdebug.c",
		}
		
		filter "system:windows"
			files
			{
				"vendor/freetype2/src/base/ftver.rc",
			}
			defines
			{
				"_CRT_SECURE_NO_WARNINGS",
				"_CRT_NONSTDC_NO_WARNINGS"
			}
			disablewarnings
			{
				"4267",
				"4477",
				"4018",
				"4244",
				"4312",
				"4701",
			}
		filter {}
			
	project "cimgui"
		language "C++"
		location("build/vendor/cimgui")
		kind "SharedLib"
		targetname "cimgui"
        targetdir "bin/%{cfg.buildcfg}"
		link_to_vulkan()
		includedirs
		{
			"vendor/glfw/include",
			"vendor/freetype2/include",
		}
		links { "glfw", "freetype2"}
		defines{"CIMGUI_SHOULD_EXPORT", "CIMGUI_FREETYPE", "IMGUI_ENABLE_FREETYPE"}
		files
		{
			"vendor/cimgui/cimgui.cpp",
			"vendor/cimgui/cimgui.h",
			"vendor/cimgui/imgui/imgui.cpp",
			"vendor/cimgui/imgui/imgui_tables.cpp",
			"vendor/cimgui/imgui/imgui_widgets.cpp",
			"vendor/cimgui/imgui/imgui_draw.cpp",
			"vendor/cimgui/imgui/imgui_demo.cpp",
			"vendor/cimgui/imgui/misc/freetype/imgui_freetype.cpp",
			"vendor/cimgui/imgui/imgui.h",
			"vendor/cimgui/imgui/imgui_internal.h",
			"vendor/cimgui/imgui/misc/freetype/imgui_freetype.h",
			"vendor/cimgui/imgui/imconfig.h",
			"vendor/cimgui/imgui/backends/imgui_impl_vulkan.cpp",
			"vendor/cimgui/imgui/backends/imgui_impl_vulkan.h",
			"vendor/cimgui/imgui/backends/imgui_impl_glfw.cpp",
			"vendor/cimgui/imgui/backends/imgui_impl_glfw.h",
		}
		

group "modules"
	fabric_module "window"
		links {"glfw"}
		includedirs{"", "vendor/glfw/include" }
		
	fabric_module "vulkan_backend"
		link_to_vulkan()
		links {"glfw", "cimgui"}
		includedirs{"vendor/glfw/include"}
			
	fabric_module "renderer"
		files{"content/shaders/*.vert", "content/shaders/*.frag", "content/shaders/*.h"}

	fabric_module "asset_import"

	fabric_module "ui"
		links{"cimgui"}
		
	fabric_module "ui_widgets"
		links{"cimgui"}
		
	fabric_module "voxel"
	
	fabric_module "world_gen"

group ""
	fabric_project "core"
		kind "StaticLib"

	fabric_module "game"
		links{"cimgui"} -- todo: this shouldn't depend on ImGui. Right now required for viewport draw
		
	main_exe "unit_test"

	main_exe "voxel-fabric"
		folder {"main"}
		
		filter { "platforms:Win64" }
			postbuildcommands
			{
				'{MKDIR} ../../bin/%{cfg.buildcfg}/content/',
				'{COPY} ../../content/ ../../bin/%{cfg.buildcfg}/content/'
			}
		filter { "platforms:MacOSX or Linux" }
			postbuildcommands
			{
				'{MKDIR} ../../bin/%{cfg.buildcfg}/content/',
				'{COPY} ../../content/ ../../bin/%{cfg.buildcfg}/'
			}
		filter {}