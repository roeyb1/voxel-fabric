#pragma once

#include <core/core_types.h>

struct fa_memory_stats_api
{
    void (*alloc_track)(void* ptr, size_t size, const char* filename, size_t line);
    void (*alloc_free)(void* ptr, const char* filename, size_t line);

    void (*log_mem_stats)(void);
};

#define FA_MEMORY_STATS_API_NAME "FA_MEMORY_STATS_API"

#if defined(FA_LINKS_TO_CORE)
extern struct fa_memory_stats_api* fa_memory_stats_api;
#endif