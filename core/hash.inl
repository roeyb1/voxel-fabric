#pragma once

#include <core/allocator.h>
#include <core/core_types.h>
#include <core/error.h>
#include <core/stretchy_buffers.inl>

static const uint64_t FA_HASH_TOMBSTONE = UINT64_MAX;
static const uint64_t FA_HASH_UNUSED = UINT64_MAX - 1;

/**
 * Usage:
 *      typedef struct FA_HASH_T(k, v) some_hash_table;
 */
#define FA_HASH_T(K, V)                                                                         \
    {                                                                                           \
        /* sbuff */ K* keys;                                                                    \
        /* keys are indices into an external data array*/                                       \
        V* values;                                                                              \
        /* store the number of keys since we might have tombstone values in the keys array.     \
           Should always be a power of two to simplify the modulus operations to bitwise and */ \
        uint64_t num_keys;                                                                      \
        uint64_t temp;                                                                          \
    }

#define fa_hash_get(hashmap, key)                                                     \
    ((hashmap)->temp = _fa_hash_get_index((hashmap)->keys, (hashmap)->num_keys, key), \
     (hashmap)->temp != UINT64_MAX ? &(hashmap)->values[(hashmap)->temp] : NULL)

#define fa_hash_add(hashmap, k, v) \
    (_fa_hash_add(&((hashmap)->keys), &((hashmap)->num_keys), (void**)(&((hashmap)->values)), k, &v, sizeof(v)))

#define fa_hash_remove(hashmap, k) \
    (_fa_hash_remove((hashmap)->keys, &((hashmap)->num_keys), k))

#define fa_hash_free(hashmap) \
    (fa_sbuff_free((hashmap)->keys), fa_free((hashmap)->values))

/* gets the index given a key. Currently using linear probing */
static inline uint64_t _fa_hash_probe(uint64_t key, uint64_t distance, uint64_t num_keys)
{
    const uint64_t base = key & (num_keys - 1);
    return base + distance;
}

static inline uint64_t _fa_hash_get_index(const uint64_t* keys, uint64_t num_keys, uint64_t key)
{
    uint64_t i = _fa_hash_probe(key, 0, num_keys);
    uint64_t distance = 0;
    const uint64_t max_distance = num_keys;
    while (keys[i] != key)
    {
        if (distance > max_distance)
        {
            return UINT64_MAX;
        }
        if (keys[i] == FA_HASH_UNUSED)
        {
            return UINT64_MAX;
        }
        ++distance;
        i = _fa_hash_probe(key, distance, num_keys);
    }

    return i;
}

static inline void _fa_hash_grow(uint64_t** keys, uint64_t* num_keys, void** values, size_t value_size)
{
    const uint64_t new_num = *num_keys ? 2 * (*num_keys) : 16;
    for (uint32_t i = 0; i < (new_num - *num_keys); ++i)
        fa_sbuff_push(*keys, FA_HASH_UNUSED);

    *num_keys = new_num;

    *values = fa_realloc(*values, new_num * value_size);
}

static inline void _fa_hash_add(uint64_t** keys, uint64_t* num_keys, void** values, uint64_t key, const void* value, size_t value_size)
{
    uint64_t i = _fa_hash_probe(key, 0, *num_keys);
    uint64_t distance = 0;
    const uint64_t max_distance = *num_keys;

    if (*keys)
    {
        while ((*keys)[i] != FA_HASH_TOMBSTONE && (*keys)[i] != FA_HASH_UNUSED)
        {
            if (distance > max_distance)
            {
                break;
            }
            ++distance;
            i = _fa_hash_probe(key, distance, *num_keys);
        }
    }
    if (distance > max_distance || !(*keys))
    {
        _fa_hash_grow(keys, num_keys, values, value_size);
    }

    i = _fa_hash_probe(key, 0, *num_keys);
    distance = 0;
    while ((*keys)[i] != FA_HASH_TOMBSTONE && (*keys)[i] != FA_HASH_UNUSED)
    {
        if (distance > max_distance)
        {
            break;
        }
        ++distance;
        i = _fa_hash_probe(key, distance, *num_keys);
    }

    check(i < fa_sbuff_num(*keys));
    (*keys)[i] = key;
    memcpy((void*)((uintptr_t)(*values) + i * value_size), value, value_size);
}

static inline bool _fa_hash_remove(uint64_t* keys, uint64_t* num_keys, uint64_t key)
{
    const uint64_t index = _fa_hash_get_index(keys, *num_keys, key);

    if (index != UINT64_MAX)
    {
        keys[index] = FA_HASH_TOMBSTONE;

        *num_keys = *num_keys - 1;

        return true;
    }
    return false;
}

typedef struct FA_HASH_T(uint64_t, uint64_t) fa_hash64_t;
typedef struct FA_HASH_T(uint64_t, float) fa_hashfloat_t;
