#if defined(FA_OS_MACOSX)
#include "os.h"

#include <unistd.h>
#include <dlfcn.h>
#include <sys/stat.h>
#include <mach/clock.h>
#include <mach/mach_time.h>

static void thread_sleep(float seconds)
{
    sleep(seconds);
}

static struct fa_os_thread_api thread_api = 
{
    .sleep = thread_sleep,
};

static fa_os_shared_lib_t load_lib(const char* filename)
{
    const void* handle = dlopen(filename, RTLD_LAZY);
    if (handle == NULL)
    {

    }
    return (fa_os_shared_lib_t)
    {
        .handle = (uint64_t)handle,
        .valid = handle != NULL,
    };
}

static void* get_symbol(fa_os_shared_lib_t lib, const char* symbol_name)
{
    if (lib.valid)
    {
        return dlsym((void*)lib.handle, symbol_name);
    }
    return NULL;
}

static void close_lib(fa_os_shared_lib_t lib)
{
    if (lib.valid)
    {
        dlclose((void*)lib.handle);
    }
}

static struct fa_os_shared_lib_api shared_lib = {
    .load_lib = load_lib,
    .get_symbol = get_symbol,
    .close_lib = close_lib,
};

static fa_file_time_t get_last_modified_time(const char* filename)
{
    (void)filename;
    return (fa_file_time_t){0};
}
static void set_last_modified_time(const char* filename, fa_file_time_t file_time)
{
    (void)filename;
    (void)file_time;
}

static uint64_t get_file_size(const char* filename)
{
    struct stat st;
    stat(filename, &st);
    return st.st_size;
}

static struct fa_os_file_api file_api = {
    .get_file_size = get_file_size,
    .get_last_modified_time = get_last_modified_time,
    .set_last_modified_time = set_last_modified_time,
};


static fa_clock_t now(void)
{
    fa_clock_t now;
    now.ticks = mach_absolute_time();
    return now;
}


static double time_sub(fa_clock_t a, fa_clock_t b)
{
    uint64_t delta = a.ticks - b.ticks;

    mach_timebase_info_data_t timebase;
    mach_timebase_info(&timebase);
    return (double)delta * (double)timebase.numer / (double)timebase.denom / 1e9;
}

static struct fa_os_time_api time_api = {
    .now = now,
    .time_sub = time_sub,
};

static struct fa_os_api os =
{
    .thread = &thread_api,
    .shared_lib = &shared_lib,
    .file = &file_api,
    .time = &time_api,
};

struct fa_os_api* fa_os_api = &os;

#endif

int foo;
