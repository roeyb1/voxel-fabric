#pragma once

// Currently this header file is just a wrapper around the Tracy macros
// In the future we might roll our own profiler so this will add a layer that we can abstract off

#if defined(FA_PROFILE)
// WARNING: Tracy does not function across fiber switches. All zones must start/end before a fiber switch (wait_on_counter)
#include <TracyC.h>

#define FA_PROFILER_ZONE(ctx, active) TracyCZone(ctx, active)
#define FA_PROFILER_ZONE_N(ctx, name, active) TracyCZoneN(ctx, name, active)
#define FA_PROFILER_ZONE_END(ctx) TracyCZoneEnd(ctx)

#define FA_PROFILER_TRACK_ALLOC(ptr, size) TracyCAlloc(ptr, size)
#define FA_PROFILER_FREE_ALLOC(ptr) TracyCFree(ptr)

#define FA_PROFILER_FRAME_MARK TracyCFrameMark

#define FA_PROFILER_SET_THREAD_NAME(name) TracyCSetThreadName(name)
#else
#define FA_PROFILER_ZONE(ctx, active)
#define FA_PROFILER_ZONE_N(ctx, name, active)
#define FA_PROFILER_ZONE_END(ctx)

#define FA_PROFILER_TRACK_ALLOC(ptr, size)
#define FA_PROFILER_FREE_ALLOC(ptr)

#define FA_PROFILER_FRAME_MARK

#define FA_PROFILER_SET_THREAD_NAME(name)
#endif