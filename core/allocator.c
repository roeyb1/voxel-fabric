#include "allocator.h"

#include "core_defines.h"
#include "error.h"
#include "log.h"
#include "memory_stats.h"
#include "profiler.h"

#include <stdlib.h>
#include <string.h>

static void* _malloc(size_t size, const char* file, size_t line)
{
    void* result = malloc(size);
    fa_memory_stats_api->alloc_track(result, size, file, line);
    return result;
}

static void* _realloc(void* ptr, size_t size, const char* file, size_t line)
{
    fa_memory_stats_api->alloc_free(ptr, file, line);
    void* result = realloc(ptr, size);
    fa_memory_stats_api->alloc_track(ptr, size, file, line);
    return result;
}

static void _free(void* ptr, const char* file, size_t line)
{
    fa_memory_stats_api->alloc_free(ptr, file, line);
    free(ptr);
}

#pragma region Temp Allocator

typedef struct block_alloc_block_t
{
    size_t max_size;
    size_t used;

    byte_t* buffer;

    struct block_alloc_block_t* next;
} block_alloc_block_t;

typedef struct fa_block_alloc_t
{
    block_alloc_block_t first;
    bool free_first_block;
} fa_block_alloc_t;

#define FA_GET_BLOCK_FREE_SPACE(block_ptr) \
    (void*)(((uintptr_t)block_ptr) + (uintptr_t)block_ptr->used)

// #todo: implement the re alloc part
static void* block_realloc(struct fa_block_alloc_t* inst, void* ptr, size_t size, const char* filename, size_t line)
{
    FA_ASSERT(inst != NULL);

    block_alloc_block_t* current_block = &inst->first;
    block_alloc_block_t* last_block = NULL;

    while (current_block != NULL)
    {
        const size_t remaining = current_block->max_size - current_block->used;
        if (remaining >= size)
        {
            void* start = (void*)((uintptr_t)current_block);
            void* result = (void*)((uintptr_t)start + (uintptr_t)(current_block->used));
            //void* result = FA_GET_BLOCK_FREE_SPACE(current_block);
            current_block->used += size;
            return result;
        }
        last_block = current_block;
        current_block = current_block->next;
    }

    // couldn't find a block that could fit our allocation, must create a new one.
    // We have to heap allocate the new buffer and we will make it the max of 2 * the size of
    // the current allocation or the default buffer size.
    const size_t newBlockSize = fa_max(2 * size, FA_TEMP_ALLOC_BUFFER_SIZE);
    void* newBuffer = _malloc(newBlockSize, filename, line);
    void* result = (void*)((uintptr_t)newBuffer + sizeof(block_alloc_block_t));

    block_alloc_block_t* new_block = newBuffer;
    *new_block = (block_alloc_block_t){
        .max_size = newBlockSize,
        .used = sizeof(block_alloc_block_t) + size,
        .buffer = newBuffer,
    };

    last_block->next = new_block;

    return result;
}

static fa_block_alloc_t* create_block_allocator_from_buffer(byte_t* buffer, size_t size)
{
#if FA_DEBUG
    // if we're in debug mode it might be useful to set the buffer to something easier to see in the debugger
    // note: I'm intentionally not setting to 0 here so that we don't mask any errors that might assume these
    // objects are 0 initialized.
    memset(buffer, 0xcd, size);
#endif
    fa_block_alloc_t* allocator = (fa_block_alloc_t*)buffer;
    *allocator = (fa_block_alloc_t){
        .first = (block_alloc_block_t){
            .max_size = size,
            .used = sizeof(fa_block_alloc_t),
            .buffer = buffer,
            .next = NULL,
        },
        .free_first_block = false,
    };

    return allocator;
}

static fa_block_alloc_t* create_block_allocator(size_t size)
{
    void* buffer = fa_malloc(size);
    fa_block_alloc_t* allocator = create_block_allocator_from_buffer(buffer, size);
    allocator->free_first_block = true;
    return allocator;
}

static void destroy_block_alloc(fa_block_alloc_t* temp_alloc, const char* filename, size_t line)
{
    // if this buffer was allocated from a buffer initially we don't want to free that buffer since it might
    // be managed by another allocator. In that case we should only free subsequent blocks.
    block_alloc_block_t* current = temp_alloc->free_first_block ? &temp_alloc->first : temp_alloc->first.next;
    while (current != NULL)
    {
        block_alloc_block_t* next = current->next;
        _free(current, filename, line);
        current = next;
    }
}

#pragma endregion

struct fa_allocator_api* fa_allocator_api = &(struct fa_allocator_api){
    .malloc = _malloc,
    .realloc = _realloc,
    .free = _free,
    .create_block_allocator_from_buffer = create_block_allocator_from_buffer,
    .create_block_allocator = create_block_allocator,
    .destroy_block_alloc = destroy_block_alloc,
    .block_realloc = block_realloc,
};
