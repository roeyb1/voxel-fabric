#pragma once

// djb2 string hash algorithm by dan bernstein: http://www.cse.yorku.ca/~oz/hash.html
static inline uint64_t fa_string_hash(const char* str)
{
    uint64_t hash = 5381;
    int c;
    while ((c = *str++))
    {
        hash = ((hash << 5) + hash) + c;
    }
    return hash;
}
