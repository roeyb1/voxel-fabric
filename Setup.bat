@echo off
pushd Scripts
python Setup.py
popd

echo Copying precommit hook to .git directory...
copy scripts\pre-commit .git\hooks

pushd content\shaders
call compile_shaders.bat
popd

echo Setup Complete
pause