#include "core/memory_stats.h"

#include <core/allocator.h>
#include <core/api_registry.h>
#include <core/application_api.h>
#include <core/job_scheduler.h>
#include <core/log.h>
#include <core/module_api.h>
#include <core/os.h>
#include <core/profiler.h>

#include <cglm/struct.h>
#include <vulkan/vulkan.h>

#include <modules/renderer/renderer.h>
#include <modules/ui/ui.h>
#include <modules/vulkan_backend/vulkan_backend.h>
#include <modules/window/window_api.h>

struct fa_window_api* window_api;
struct fa_vulkan_backend_api* vulkan_backend_api;
struct fa_renderer_api* renderer_api;
struct fa_job_scheduler_api* job_api;
struct fa_application_api* app_api;
struct fa_ui_api* ui_api;

// Initialize the core engine subsystems
static void init_engine()
{
    // Initialize and register the core apis
    fa_add_api(fa_api_registry, fa_os_api, FA_OS_API_NAME);
    fa_add_api(fa_api_registry, fa_logger_api, FA_LOGGER_API_NAME);
    fa_add_api(fa_api_registry, fa_allocator_api, FA_ALLOCATOR_API_NAME);

    // there currently isn't a fiber api for non-win32 platforms so we can't use the job system there
#if defined(FA_OS_WINDOWS)
    job_api = fa_init_job_scheduler(fa_os_api->thread, 8, 128, 10 * 1024);
    fa_add_api(fa_api_registry, job_api, FA_JOB_API_NAME);
#endif

    fa_module_api->load_module("fa_asset_import");

    // Load the core modules
    // #todo: figure out a way to have these all load in order and on their own.
    fa_module_api->load_module("fa_window");
    window_api = fa_api_registry->get(FA_WINDOW_API_NAME);
    window_api->init();
    const fa_window_t window = window_api->create_window(1280, 720, "Fabric");
    window_api->set_window_icon(window, "content/textures/logo_no_text.png");

    fa_module_api->load_module("fa_vulkan_backend");
    vulkan_backend_api = fa_api_registry->get(FA_VULKAN_BACKEND_API_NAME);
    vulkan_backend_api->create();

    fa_module_api->load_module("fa_renderer");
    renderer_api = fa_api_registry->get(FA_RENDERER_API_NAME);
    renderer_api->create();

    fa_module_api->load_module("fa_ui");
    ui_api = fa_api_registry->get(FA_UI_API_NAME);
    ui_api->create();

    fa_module_api->load_module("fa_ui_widgets");

    // Load the application module now
    fa_module_api->load_module("fa_game");
    app_api = fa_api_registry->get(FA_APPLICATION_API_NAME);
    app_api->create();
}

// Shutdown the core engine subsystems and clean up
static void shutdown_engine()
{
    app_api->destroy();
    fa_module_api->unload_module("fa_game");

    fa_module_api->unload_module("fa_ui_widgets");

    ui_api->destroy();
    fa_module_api->unload_module("fa_ui");

    renderer_api->destroy();
    fa_module_api->unload_module("fa_renderer");

    vulkan_backend_api->destroy();
    fa_module_api->unload_module("fa_vulkan_backend");

    window_api->shutdown();
    fa_module_api->unload_module("fa_window");

    fa_module_api->unload_module("fa_asset_import");

    fa_shutdown_job_scheduler();
    // print the memory stats when the engine is finished running so we can see if there were any leaks.
    fa_memory_stats_api->log_mem_stats();
}

static void run_main_loop()
{
    fa_application_t* app = app_api->get();

    while (app_api->tick(app))
    {
        FA_PROFILER_FRAME_MARK
        fa_module_api->check_hotreload();
    }
}

int main(int argc, char** argv)
{
    init_engine();

    run_main_loop();

    shutdown_engine();
    return 0;
}
