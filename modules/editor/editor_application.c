#include <core/api_registry.h>
#include <core/application_api.h>
#include <core/core_defines.h>
#include <core/core_types.h>
#include <core/log.h>
#include <core/module_api.h>
#include <core/os.h>
#include <core/profiler.h>

#include <modules/ui/ui.h>
#include <modules/window/window_api.h>

#define CIMGUI_DEFINE_ENUMS_AND_STRUCTS
#include <cglm/struct.h>
#include <vendor/cimgui/cimgui.h>
#include <vendor/iconfontheaders/IconsFontAwesome5.h>
#include <vulkan/vulkan.h>

#include <modules/asset_import/asset_import.h>
#include <modules/renderer/renderer.h>
#include <modules/ui/style_colors.inl>
#include <modules/ui_widgets/ui_widgets.h>
#include <modules/vulkan_backend/vulkan_backend.h>

static struct fa_logger_api* fa_logger_api;
static struct fa_window_api* fa_window_api;
static struct fa_vulkan_backend_api* fa_vulkan_backend_api;
static struct fa_renderer_api* fa_renderer_api;
static struct fa_ui_api* fa_ui_api;
static struct fa_ui_widgets_api* fa_widgets_api;
static struct fa_os_api* fa_os_api;
static struct fa_asset_import_api* fa_asset_import_api;

// Uncomment this line to enable conservative viewport resizing (only resizes the framebuffer when
// the manual resize operation finished
#define DISABLE_CONSERVATIVE_RESIZE 1

typedef struct fa_application_t
{
    // #todo: multiple windows
    /** Store a handle to the main editor window */
    fa_window_t window_handle;

    fa_render_backend_t* render_backend;

    fa_clock_t clock;
    double time;
    double delta_time;
} fa_application_t;

static fa_application_t* editor;

// temporary function to block out the editor layout until each panel is implemented
static void draw_template_panels(void)
{
    FA_PROFILER_ZONE(ctx, true);
    const ImGuiStyle* style = igGetStyle();

    // Draw Asset Panel
    {
        FA_PROFILER_ZONE_N(ctx1, "asset_panel", true);
        igPushStyleColor_Vec4(ImGuiCol_Separator, surface_color);
        if (igBegin(ICON_FA_FOLDER_TREE " Assets", NULL, 0))
        {
            const ImGuiID dockspace = igGetID_Str("AssetDockspace");

            static ImGuiID center = 0, left = 0, right = 0;

            if (!igDockBuilderGetNode(dockspace))
            {
                igDockBuilderRemoveNode(dockspace);
                igDockBuilderAddNode(dockspace, ImGuiDockNodeFlags_None);
                igDockBuilderSplitNode(dockspace, ImGuiDir_Right, 0.8f, &right, &left);
                igDockBuilderDockWindow("AssetFiles", left);
                igDockBuilderDockWindow("AssetThumbnails", center);

                igDockBuilderFinish(dockspace);
            }

            igDockSpace(dockspace, (ImVec2){ 0, 0 }, ImGuiDockNodeFlags_NoDocking | ImGuiDockNodeFlags_NoTabBar, NULL);

            static float item_size = 128.f;
            {
                if (igBegin("AssetFiles", NULL, 0))
                {
                    if (igTreeNodeEx_Str(ICON_FA_FOLDER " Content", ImGuiTreeNodeFlags_DefaultOpen))
                    {
                        if (igTreeNodeEx_Str(ICON_FA_FOLDER " Fonts", ImGuiTreeNodeFlags_Leaf))
                        {
                            igTreePop();
                        }
                        if (igTreeNodeEx_Str(ICON_FA_FOLDER " Shaders", ImGuiTreeNodeFlags_Leaf))
                        {
                            igTreePop();
                        }
                        if (igTreeNodeEx_Str(ICON_FA_FOLDER " Textures", ImGuiTreeNodeFlags_Leaf))
                        {
                            igTreePop();
                        }
                        igTreePop();
                    }
                }
                igEnd();

                if (igBegin("AssetThumbnails", NULL, 0))
                {
                    static char filter_buff[128];
                    igText(ICON_FA_FILTER);
                    igSameLine(0, 5);

                    igSetNextItemWidth(256);
                    igPushFont(fa_ui_api->get_small_font());
                    igInputTextWithHint("##AssetFilter", "Search (Ctrl + F)", filter_buff, 64, 0, NULL, NULL);
                    igPopFont();

                    const float item_width = 128;
                    ImVec2 content_region;
                    igGetContentRegionAvail(&content_region);
                    igSameLine(content_region.x - (item_width), 0);
                    igPushItemWidth(item_width);
                    igSliderFloat("##ItemSize", &item_size, 32.f, 256.f, "%1.f", 0);

                    if (igBeginChild_Str("##AssetFilesTable", IM_VEC2(0, 0), false, 0))
                    {
                        ImVec2 region;
                        igGetContentRegionAvail(&region);
                        const ImVec2 item_extent = { item_size, item_size };
                        const ImVec2 cell_size = {
                            .x = item_extent.x + style->ItemSpacing.x,
                            .y = item_extent.y + style->ItemSpacing.y,
                        };
                        const uint32_t num_columns = fa_min(fa_max((uint32_t)(region.x / cell_size.x), 1), 64);

                        if (igBeginTable("Assets", num_columns, ImGuiTableFlags_PadOuterX, IM_VEC2(0, 0), 10.f))
                        {
                            for (uint32_t i = 0; i < 500; ++i)
                            {
                                igTableNextColumn();
                                igButton(ICON_FA_FILE, item_extent);

                                ImVec2 text_size;
                                igCalcTextSize(&text_size, "file.png", NULL, false, -1.f);

                                igSetCursorPosX(igGetCursorPosX() + (item_extent.x - text_size.x) / 2);
                                igText("file.png");
                            }
                            igEndTable();
                        }
                    }
                    igEndChild();
                }
                igEnd();
            }
        }

        igEnd();
        igPopStyleColor(1);
        FA_PROFILER_ZONE_END(ctx1);
    }

    // Draw Log
    {
        FA_PROFILER_ZONE_N(ctx1, "log", true);
        igBegin(ICON_FA_COMMENT_ALT " Log", NULL, 0);

        igEnd();
        FA_PROFILER_ZONE_END(ctx1);
    }

    // Draw Property Editor
    {
        FA_PROFILER_ZONE_N(ctx1, "property_editor", true);
        if (igBegin(ICON_FA_INFO_CIRCLE " Properties", NULL, 0))
        {
            const ImGuiTableFlags table_flags = ImGuiTableFlags_Resizable;
            static char name_buffer[64] = { 'C', 'u', 'b', 'e' };
            ImVec2 content_region;
            igGetContentRegionAvail(&content_region);
            igSetNextItemWidth(content_region.x);
            igInputText("##PropertyEditorEntityName", name_buffer, 64, 0, NULL, NULL);

            const ImGuiTreeNodeFlags treenode_flags = ImGuiTreeNodeFlags_DefaultOpen;
            if (igCollapsingHeader_TreeNodeFlags("Transform", treenode_flags))
            {
                static vec3s position = { 0, 0, 0 };
                static vec3s rotation = { 0, 0, 0 };
                static vec3s scale = { 1, 1, 1 };

                if (igBeginTable("Properties", 2, table_flags, IM_VEC2(0, 0), 0.f))
                {
                    fa_widgets_api->draw_transform(&position, &rotation, &scale);
                    igEndTable();
                }
            }

            if (igCollapsingHeader_TreeNodeFlags("Material", treenode_flags))
            {
                if (igBeginTable("Properties", 2, table_flags, IM_VEC2(0, 0), 0.f))
                {
                    igTableNextColumn();
                    igIndent(2 * style->IndentSpacing);
                    igSetCursorPosY(igGetCursorPosY() + style->FramePadding.x);
                    igText("Shader");
                    igUnindent(2 * style->IndentSpacing);

                    igTableNextColumn();

                    static int current_item = 0;
                    igSetNextItemWidth(-1.f);
                    igCombo_Str("##ShaderCombo", &current_item, "Standard\0Flat", 4);
                    igEndTable();
                }
            }

            // Add Component button
            {
                ImVec2 window_pos;
                igGetWindowPos(&window_pos);
                ImDrawList_AddRect(igGetWindowDrawList(), IM_VEC2(window_pos.x + igGetCursorPosX(), window_pos.y + igGetCursorPosY() + igGetTextLineHeightWithSpacing() / 2), IM_VEC2(window_pos.x + content_region.x, window_pos.y + igGetCursorPosY() + igGetTextLineHeightWithSpacing() / 2), CIM_COL32_BLACK, 5.f, 0, 1.f);
                igText("");

                ImVec2 text_size;
                const char* add_component_text = ICON_FA_PLUS " Add Component";
                igCalcTextSize(&text_size, add_component_text, NULL, false, -1.f);
                text_size.x += 8 * style->FramePadding.x;
                const float button_pos = (content_region.x - (text_size.x)) / 2;

                igSetCursorPosX(button_pos);
                igButton(add_component_text, IM_VEC2(text_size.x, 0));
            }
        }

        igEnd();
        FA_PROFILER_ZONE_END(ctx1);
    }

    // Draw Scene Tree
    {
        FA_PROFILER_ZONE_N(ctx1, "scene_tree", true);
        igBegin(ICON_FA_CUBES " Scene Tree", NULL, ImGuiWindowFlags_MenuBar);
        igBeginMenuBar();

        // filter
        {
            igText(ICON_FA_FILTER);
            igSameLine(0, 8);
            static char buff[64];
            igPushFont(fa_ui_api->get_small_font());
            igInputTextWithHint("##SceneTreeSearch", "Search (Ctrl + F)", buff, 64, 0, NULL, NULL);
            igPopFont();
        }

        igEndMenuBar();

        const ImGuiTreeNodeFlags base_flags = ImGuiTreeNodeFlags_SpanFullWidth | ImGuiTreeNodeFlags_Leaf;
        static bool selection[] = { false, false, false, false, false, false, false, false };
        if (igTreeNodeEx_Str(ICON_FA_GLOBE " Scene", ImGuiTreeNodeFlags_DefaultOpen))
        {
            for (uint32_t i = 0; i < 6; ++i)
            {
                ImGuiTreeNodeFlags node_flags = base_flags;
                if (selection[i])
                {
                    node_flags |= ImGuiTreeNodeFlags_Selected;
                }
                const bool is_open = igTreeNodeEx_Ptr((void*)(intptr_t)i, node_flags, ICON_FA_CUBE " entity %d", i);
                if (igIsItemClicked(ImGuiMouseButton_Left))
                {
                    selection[i] = !selection[i];
                }

                if (is_open)
                {
                    igTreePop();
                }
            }
            igTreePop();
        }

        igEnd();
        FA_PROFILER_ZONE_END(ctx1);
    }

    igShowDemoWindow(NULL);
    FA_PROFILER_ZONE_END(ctx);
}

static void draw_main_menubar(void)
{
    FA_PROFILER_ZONE(ctx, true);
    igPushStyleColor_Vec4(ImGuiCol_MenuBarBg, surface_color);
    igPushStyleColor_U32(ImGuiCol_Border, CIM_COL32_BLACK_TRANS);
    igBeginMainMenuBar();
    igPopStyleColor(2);

    igPushStyleVar_Vec2(ImGuiStyleVar_WindowPadding, (ImVec2){ 15, 15 });
    igPushStyleColor_Vec4(ImGuiCol_PopupBg, active);
    igPushStyleColor_Vec4(ImGuiCol_Border, (ImVec4){ 0.4f, 0.4f, 0.4f, 1.f });

    if (igBeginMenu("File", true))
    {
        igText("TODO");
        igText("TODO");
        igText("TODO");
        igText("TODO");
        igText("TODO");
        igEndMenu();
    }

    if (igBeginMenu("Edit", true))
    {
        igText("TODO");
        igText("TODO");
        igText("TODO");
        igText("TODO");
        igText("TODO");
        igEndMenu();
    }

    static bool style_editor_open = false;
    if (igBeginMenu("Tools", true))
    {
        if (igMenuItem_BoolPtr("Style Editor", NULL, &style_editor_open, true))
        {
        }
        igText("TODO");
        igText("TODO");
        igText("TODO");
        igText("TODO");
        igEndMenu();
    }

    if (igBeginMenu("Windows", true))
    {
        igEndMenu();
    }

    igPopStyleVar(1);
    igPopStyleColor(2);

    // NOTE: pop all style vars/colors before showing the style editor to prevent
    // hiding the true style values.
    if (style_editor_open)
    {
        igBegin(ICON_FA_IMAGE " Style Editor", &style_editor_open, 0);
        igShowStyleEditor(NULL);
        igEnd();
    }

    igEndMainMenuBar();
    FA_PROFILER_ZONE_END(ctx);
}

static void draw_status_bar(void)
{
    FA_PROFILER_ZONE(ctx, true);
    ImGuiViewport* viewport = igGetMainViewport();

    // Notify of viewport change so GetFrameHeight() can be accurate in case of DPI change
    igSetCurrentViewport(NULL, (ImGuiViewportP*)viewport);

    igPushStyleColor_Vec4(ImGuiCol_MenuBarBg, primary);

    ImVec2 size;
    igCalcTextSize(&size, "8", NULL, true, 1.f);
    if (igBeginViewportSideBar("StatusBar", viewport, ImGuiDir_Down, size.y + 2 * igGetStyle()->FramePadding.y, ImGuiWindowFlags_NoScrollbar | ImGuiWindowFlags_NoSavedSettings | ImGuiWindowFlags_MenuBar))
    {
        if (igBeginMenuBar())
        {
            igText("%.2f ms", editor->delta_time * 1000.f);
            igEndMenuBar();
        }
        igEnd();
    }
    igPopStyleColor(1);
    FA_PROFILER_ZONE_END(ctx);
}

static void draw_dockspace(void)
{
    FA_PROFILER_ZONE(ctx, true);
    static ImGuiDockNodeFlags DockspaceFlags = ImGuiDockNodeFlags_None;

    // We are using the ImGuiWindowFlags_NoDocking flag to make the parent window not dockable into,
    // because it would be confusing to have two docking targets within each others.
    ImGuiWindowFlags WindowFlags = ImGuiWindowFlags_MenuBar | ImGuiWindowFlags_NoDocking;
    const ImGuiViewport* ImGuiMainViewport = igGetMainViewport();
    igSetNextWindowPos(ImGuiMainViewport->Pos, 0, (ImVec2){ 0, 0 });
    igSetNextWindowSize(ImGuiMainViewport->Size, 0);
    igSetNextWindowViewport(ImGuiMainViewport->ID);
    igPushStyleVar_Float(ImGuiStyleVar_WindowRounding, 0.0f);
    igPushStyleVar_Float(ImGuiStyleVar_WindowBorderSize, 0.0f);
    igPushStyleColor_Vec4(ImGuiCol_MenuBarBg, (ImVec4){ 0.14f, 0.14f, 0.14f, 1.00f });
    WindowFlags |= ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoMove;
    WindowFlags |= ImGuiWindowFlags_NoBringToFrontOnFocus | ImGuiWindowFlags_NoNavFocus;

    // When using ImGuiDockNodeFlags_PassthruCentralNode, DockSpace() will render our background and handle the pass-thru hole, so we ask Begin() to not render a background.
    if (DockspaceFlags & ImGuiDockNodeFlags_PassthruCentralNode)
    {
        WindowFlags |= ImGuiWindowFlags_NoBackground;
    }

    // Important: note that we proceed even if Begin() returns false (aka window is collapsed).
    // This is because we want to keep our DockSpace() active. If a DockSpace() is inactive,
    // all active windows docked into it will lose their parent and become undocked.
    // We cannot preserve the docking relationship between an active window and an inactive docking, otherwise
    // any change of dockspace/settings would lead to windows being stuck in limbo and never being visible.
    igPushStyleVar_Vec2(ImGuiStyleVar_WindowPadding, (ImVec2){ 0.0f, 0.0f });
    igBegin("DockSpace", NULL, WindowFlags);

    igPopStyleVar(1);
    igPopStyleColor(1);
    igPopStyleVar(2);

    const ImGuiID DockspaceID = igGetID_Str("DockSpace");
    igDockSpace(DockspaceID, (ImVec2){ 0.0f, 0.0f }, DockspaceFlags, NULL);

    igEnd();
    FA_PROFILER_ZONE_END(ctx);
}

static bool scene_color_needs_resize = false;
static vec2s scene_framebuffer_size = { { 1280, 720 } };

static void draw_scene_viewport(void)
{
    FA_PROFILER_ZONE(ctx, true);
    const uint64_t scene_color_descriptor = fa_vulkan_backend_api->get_scene_color_descriptor();

    igBegin(ICON_FA_HASHTAG " Scene", NULL, 0);

    ImVec2 current_scene_panel_size;
    igGetContentRegionAvail(&current_scene_panel_size);

    // Resize when our framebuffer size is not equal to the current framebuffer size.
    // Wait until the mouse is released before triggered the resize so we don't end up
    // resizing every frame along the way.
    if ((current_scene_panel_size.x > 0 && current_scene_panel_size.y > 0) &&
        (scene_framebuffer_size.x != current_scene_panel_size.x ||
         scene_framebuffer_size.y != current_scene_panel_size.y))
    {
        if (FA_IS_DEFINED(DISABLE_CONSERVATIVE_RESIZE) || !igIsMouseDown(ImGuiMouseButton_Left))
        {
            scene_color_needs_resize = true;
            scene_framebuffer_size.x = current_scene_panel_size.x;
            scene_framebuffer_size.y = current_scene_panel_size.y;
        }
    }

    const float cursor_pos_y = igGetCursorPosY();
    ImVec2 content_region;
    igGetContentRegionAvail(&content_region);

    igImage((ImTextureID)scene_color_descriptor,
            current_scene_panel_size,
            (ImVec2){ 0.f, 0.f },
            (ImVec2){ 1.f, 1.f },
            (ImVec4){ 1.f, 1.f, 1.f, 1.f },
            (ImVec4){ 0.f, 0.f, 0.f, 0.f });

    // menu buttons
    {
        const ImGuiStyle* style = igGetStyle();
        ImVec2 text_size;
        igCalcTextSize(&text_size, ICON_FA_PLAY, NULL, false, -1);

        const float button_size = text_size.x + 4 * style->FramePadding.x;
        const float center_pos = content_region.x / 2;

        igSetCursorPos(IM_VEC2(center_pos - button_size, cursor_pos_y + style->FramePadding.y));

        ImVec4 button_col = overlay_4;
        button_col.w = 0.5f;
        igPushStyleColor_Vec4(ImGuiCol_Button, button_col);
        igButton(ICON_FA_PLAY, (ImVec2){ button_size, 0.f });
        igSameLine(0, 2 * style->FramePadding.x);
        igButton(ICON_FA_PAUSE, (ImVec2){ button_size, 0.f });
        igPopStyleColor(1);
    }

    igEnd();
    FA_PROFILER_ZONE_END(ctx);
}

#pragma region Application Api Implementation

static bool editor_tick(fa_application_t* app)
{
    const fa_window_t window = app->window_handle;

    // Update frame time and delta time
    const fa_clock_t now = fa_os_api->time->now();
    const double delta_time = fa_os_api->time->time_sub(now, app->clock);

    app->clock = now;
    app->delta_time = delta_time;
    app->time += delta_time;

    fa_window_api->update_window(window);
    fa_renderer_api->frame_begin();

    draw_dockspace();

    draw_main_menubar();

    draw_status_bar();

    draw_template_panels();

    draw_scene_viewport();

    fa_renderer_api->frame_draw();

    if (scene_color_needs_resize)
    {
        fa_vulkan_backend_api->resize_scene_color_framebuffer((uint32_t)scene_framebuffer_size.x, (uint32_t)scene_framebuffer_size.y);
        scene_color_needs_resize = false;
    }

    return !fa_window_api->window_wants_close(window);
}

static void editor_destroy(void)
{
}

static fa_application_t* editor_create(void)
{
    vertex_t* sphere_vertices = NULL;
    uint32_t* sphere_indices = NULL;
    fa_asset_import_api->load_obj("content/models/sphere.obj", &sphere_vertices, &sphere_indices);

    fa_material_t* rustediron_material = fa_renderer_api->material_create();
    rustediron_material->albedo = fa_renderer_api->texture_create("content/textures/rustediron/albedo.png");
    rustediron_material->metallic = fa_renderer_api->texture_create("content/textures/rustediron/metalness.png");
    rustediron_material->roughness = fa_renderer_api->texture_create("content/textures/rustediron/roughness.png");
    rustediron_material->normal = fa_renderer_api->texture_create("content/textures/rustediron/normal.png");
    rustediron_material->ao = fa_renderer_api->texture_create("content/textures/white.png");

    fa_material_t* red_plastic = fa_renderer_api->material_create();
    red_plastic->albedo = fa_renderer_api->texture_create("content/textures/red-plastic/albedo.png");
    red_plastic->metallic = fa_renderer_api->texture_create("content/textures/red-plastic/metalness.png");
    red_plastic->roughness = fa_renderer_api->texture_create("content/textures/red-plastic/roughness.png");
    red_plastic->normal = fa_renderer_api->texture_create("content/textures/red-plastic/normal.png");
    red_plastic->ao = fa_renderer_api->texture_create("content/textures/white.png");

    fa_material_t* bamboo_wood = fa_renderer_api->material_create();
    bamboo_wood->albedo = fa_renderer_api->texture_create("content/textures/bamboo-wood/albedo.png");
    bamboo_wood->metallic = fa_renderer_api->texture_create("content/textures/bamboo-wood/metalness.png");
    bamboo_wood->roughness = fa_renderer_api->texture_create("content/textures/bamboo-wood/roughness.png");
    bamboo_wood->normal = fa_renderer_api->texture_create("content/textures/bamboo-wood/normal.png");
    bamboo_wood->ao = fa_renderer_api->texture_create("content/textures/bamboo-wood/ao.png");
    {
        fa_mesh_t* sphere_mesh = fa_renderer_api->mesh_create(sphere_vertices, sphere_indices);
        sphere_mesh->transform = glms_rotate(sphere_mesh->transform, glm_rad(-90.f), (vec3s){ 1.f, 0.f, 0.f });
        sphere_mesh->transform = glms_translate(sphere_mesh->transform, (vec3s){ 0.0f, 0.f, 0.f });
        sphere_mesh->material = rustediron_material;
        fa_mesh_t* sphere_mesh2 = fa_renderer_api->mesh_create(sphere_vertices, sphere_indices);
        sphere_mesh2->transform = glms_rotate(sphere_mesh2->transform, glm_rad(-90.f), (vec3s){ 1.f, 0.f, 0.f });
        sphere_mesh2->transform = glms_translate(sphere_mesh2->transform, (vec3s){ 2.5f, 0.f, 0.f });
        sphere_mesh2->material = red_plastic;
        fa_mesh_t* sphere_mesh3 = fa_renderer_api->mesh_create(sphere_vertices, sphere_indices);
        sphere_mesh3->transform = glms_rotate(sphere_mesh3->transform, glm_rad(-90.f), (vec3s){ 1.f, 0.f, 0.f });
        sphere_mesh3->transform = glms_translate(sphere_mesh3->transform, (vec3s){ -2.5f, 0.f, 0.f });
        sphere_mesh3->material = bamboo_wood;
    }

    {
        fa_point_light_t* pl = fa_renderer_api->light_point_create();
        pl->pos = (vec3s){ -5.f, 4.f, 1.f };
        pl->color = (vec3s){ 200.f, 200.f, 200.f };

        fa_point_light_t* pl2 = fa_renderer_api->light_point_create();
        pl2->pos = (vec3s){ 0.f, 0.f, 4.f };
        pl2->color = (vec3s){ 150.f, 150.f, 150.f };
    }

    editor->window_handle = fa_window_api->get_window();
    return (fa_application_t*)&editor;
}

static fa_application_t* editor_get(void)
{
    return (fa_application_t*)editor;
}

static void editor_exit(fa_application_t* app)
{
    fa_ui_api->destroy();

    fa_window_api->close_window(app->window_handle);
}

#pragma endregion

#pragma region Module

static fa_application_api editor_application_api = {
    .create = editor_create,
    .get = editor_get,
    .destroy = editor_destroy,
    .exit = editor_exit,
    .tick = editor_tick,
};

FA_LIB_EXPORT void fa_load_module(struct fa_api_registry* registry, bool load)
{
    fa_logger_api = registry->get(FA_LOGGER_API_NAME);
    fa_window_api = registry->get(FA_WINDOW_API_NAME);
    fa_vulkan_backend_api = registry->get(FA_VULKAN_BACKEND_API_NAME);
    fa_renderer_api = registry->get(FA_RENDERER_API_NAME);
    fa_ui_api = registry->get(FA_UI_API_NAME);
    fa_widgets_api = registry->get(FA_UI_WIDGETS_API_NAME);
    fa_os_api = registry->get(FA_OS_API_NAME);
    fa_asset_import_api = registry->get(FA_ASSET_IMPORT_API_NAME);

    fa_add_or_remove_api(&editor_application_api, FA_APPLICATION_API_NAME, load);

    editor = registry->static_variable(FA_APPLICATION_API_NAME, "editor", sizeof(fa_application_t));
}

#pragma endregion