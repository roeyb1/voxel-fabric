#include <cglm/struct.h>

#include "ui_widgets.h"

#include <core/api_registry.h>
#include <core/core_defines.h>

#include <modules/ui/ui.h>

#define CIMGUI_DEFINE_ENUMS_AND_STRUCTS
#include <vendor/cimgui/cimgui.h>

static struct fa_ui_api* fa_ui_api;

static void draw_vec3(const char* label, vec3s* v)
{
    const ImGuiStyle* style = igGetStyle();

    igPushID_Str(label);
    igTableNextColumn();
    igIndent(2 * style->IndentSpacing);
    igSetCursorPosY(igGetCursorPosY() + style->FramePadding.x);
    igText(label);
    igUnindent(2 * style->IndentSpacing);

    igTableNextColumn();

    ImVec2 avail_size;
    igGetContentRegionAvail(&avail_size);

    ImVec2 button_size;
    igCalcTextSize(&button_size, "X", NULL, false, -1.f);
    button_size.y += 2 * style->FramePadding.y;
    button_size.x = button_size.y;

    const float slider_width = (avail_size.x - (3 * (button_size.x) + style->ItemSpacing.x)) / 3;

    igPushStyleColor_Vec4(ImGuiCol_Button, IM_VEC4(0.8f, 0.141f, 0.113f, 1.f));
    igButton("x", button_size);
    igPopStyleColor(1);
    igSameLine(0, 0);
    igSetNextItemWidth(slider_width);
    igDragFloat("##Vec3X", &v->x, 0.1f, FLT_MIN, FLT_MAX, "%.3f", 0);
    igSameLine(0, style->ItemSpacing.x / 2);

    igPushStyleColor_Vec4(ImGuiCol_Button, IM_VEC4(0.596f, 0.592f, 0.101f, 1.f));
    igButton("y", button_size);
    igPopStyleColor(1);
    igSameLine(0, 0);
    igSetNextItemWidth(slider_width);
    igDragFloat("##Vec3Y", &v->y, 0.1f, FLT_MIN, FLT_MAX, "%.3f", 0);
    igSameLine(0, style->ItemSpacing.x / 2);

    igPushStyleColor_Vec4(ImGuiCol_Button, IM_VEC4(0.270f, 0.521f, 0.533f, 1.f));
    igButton("z", button_size);
    igPopStyleColor(1);
    igSameLine(0, 0);
    igSetNextItemWidth(slider_width);
    igDragFloat("##Vec3Z", &v->z, 0.1f, FLT_MIN, FLT_MAX, "%.3f", 0);

    igPopID();
}
static void draw_transform(vec3s* position, vec3s* rotation, vec3s* scale)
{
    draw_vec3("Position", position);
    draw_vec3("Rotation", rotation);
    draw_vec3("Scale", scale);
}

static struct fa_ui_widgets_api widgets_api = {
    .draw_transform = draw_transform,
};

FA_LIB_EXPORT void fa_load_module(struct fa_api_registry* registry, bool load)
{
    fa_ui_api = registry->get(FA_UI_API_NAME);
    fa_add_or_remove_api(&widgets_api, FA_UI_WIDGETS_API_NAME, load);
}
