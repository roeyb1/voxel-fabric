#include "asset_import.h"
#include <core/api_registry.h>
#include <core/core_defines.h>
#include <core/core_types.h>
#include <core/log.h>
#include <core/stretchy_buffers.inl>

#include <cglm/struct.h>
#include <stdio.h>
#include <string.h>

#include <modules/renderer/renderer.h>

#define LOG_CATEGORY "Asset Import"

static struct fa_logger_api* fa_logger_api;

static void load_obj(const char* filename, vertex_t** out_vertices, uint32_t** out_indices)
{
    *out_vertices = NULL;
    *out_indices = NULL;
}

static struct fa_asset_import_api import_api = {
    .load_obj = load_obj,
};

FA_LIB_EXPORT void fa_load_module(struct fa_api_registry* registry, bool load)
{
    fa_logger_api = registry->get(FA_LOGGER_API_NAME);

    fa_add_or_remove_api(&import_api, FA_ASSET_IMPORT_API_NAME, load);
}