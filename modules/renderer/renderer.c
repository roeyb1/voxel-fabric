#include <cglm/struct.h>

#include "renderer.h"

#include <core/allocator.h>
#include <core/api_registry.h>
#include <core/array.h>
#include <core/core_defines.h>
#include <core/error.h>
#include <core/job_scheduler.h>
#include <core/log.h>
#include <core/profiler.h>
#include <core/stretchy_buffers.inl>
#include <vulkan/vulkan.h>

#include <modules/voxel/voxel_module.h>
#include <modules/vulkan_backend/vulkan_backend.h>

#include <string.h>
#define STB_IMAGE_IMPLEMENTATION
#include <vendor/stb/stb_image.h>

#define LOG_CATEGORY "Renderer"

static struct fa_vulkan_backend_api* fa_vulkan_backend_api;
static struct fa_logger_api* fa_logger_api;
static struct fa_allocator_api* fa_allocator_api;
static struct fa_job_scheduler_api* fa_job_scheduler_api;
static struct fa_voxel_api* fa_voxel_api;

#define VOXEL_CHUNK_WIDTH 8

typedef struct scene_t
{
    /* sbuff */ VkDrawIndexedIndirectCommand* draw_command_data;
} scene_t;

typedef struct renderer_t
{
    scene_t scene;
    fa_gpu_scene_t gpu_scene;

    uint32_t frame_index;
} renderer_t;

static renderer_t* renderer;

static void frame_begin(void)
{
    FA_PROFILER_ZONE(ctx, true);

    renderer->frame_index = fa_vulkan_backend_api->begin_frame();

    FA_PROFILER_ZONE_END(ctx);
}

static void frame_draw(void)
{
    FA_PROFILER_ZONE(ctx, true);
    const uint32_t frame_index = renderer->frame_index;

    fa_gpu_scene_t* gpu_scene = &renderer->gpu_scene;

    // Upload the scene data to the gpu scene
    // #todo [roey]: only update the parts of these arrays which have changed since last frame
    if (gpu_scene->draw_data_dirty[frame_index])
    {
        const fa_voxel_world_t* world = fa_voxel_api->get_world();
        if ((gpu_scene->voxel_chunk_buffer[frame_index].buffer == VK_NULL_HANDLE))
        {
            fa_vulkan_backend_api->buffer_free(gpu_scene->voxel_chunk_buffer[frame_index]);
            gpu_scene->voxel_chunk_buffer[frame_index] = fa_vulkan_backend_api->buffer_alloc(sizeof(world->chunk_data), VK_BUFFER_USAGE_STORAGE_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, "VoxelChunkData");
        }
        if ((gpu_scene->voxel_chunk_indices[frame_index].buffer == VK_NULL_HANDLE))
        {
            fa_vulkan_backend_api->buffer_free(gpu_scene->voxel_chunk_indices[frame_index]);
            gpu_scene->voxel_chunk_indices[frame_index] = fa_vulkan_backend_api->buffer_alloc(sizeof(world->chunk_indices), VK_BUFFER_USAGE_STORAGE_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, "VoxelChunkIndices");
        }
        if ((gpu_scene->voxel_color_buffer[frame_index].buffer == VK_NULL_HANDLE))
        {
            fa_vulkan_backend_api->buffer_free(gpu_scene->voxel_color_buffer[frame_index]);
            gpu_scene->voxel_color_buffer[frame_index] = fa_vulkan_backend_api->buffer_alloc(sizeof(world->voxel_colors), VK_BUFFER_USAGE_STORAGE_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, "VoxelColors");
        }
        gpu_scene->draw_data_dirty[frame_index] = false;

        fa_vulkan_backend_api->buffer_upload(gpu_scene->voxel_chunk_buffer[frame_index], &world->chunk_data, 0, sizeof(world->chunk_data));
        fa_vulkan_backend_api->buffer_upload(gpu_scene->voxel_chunk_indices[frame_index], &world->chunk_indices, 0, sizeof(world->chunk_indices));
        fa_vulkan_backend_api->buffer_upload(gpu_scene->voxel_color_buffer[frame_index], &world->voxel_colors, 0, sizeof(world->voxel_colors));

        VkDescriptorSet descriptor_set = fa_vulkan_backend_api->descriptor_get_bindless_set();

        VkDescriptorBufferInfo voxel_chunks_buffer = {
            .buffer = gpu_scene->voxel_chunk_buffer[frame_index].buffer,
            .offset = 0,
            .range = sizeof(world->chunk_data),
        };

        const VkWriteDescriptorSet voxel_chunks_write = {
            .sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
            .dstSet = descriptor_set,
            .dstBinding = 0,
            .dstArrayElement = 0,
            .descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,
            .descriptorCount = 1,
            .pBufferInfo = &voxel_chunks_buffer,
            .pImageInfo = NULL,
            .pTexelBufferView = NULL,
        };

        VkDescriptorBufferInfo voxel_chunk_indices = {
            .buffer = gpu_scene->voxel_chunk_indices[frame_index].buffer,
            .offset = 0,
            .range = sizeof(world->chunk_indices),
        };

        const VkWriteDescriptorSet voxel_chunks_indices_write = {
            .sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
            .dstSet = descriptor_set,
            .dstBinding = 0,
            .dstArrayElement = 2,
            .descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,
            .descriptorCount = 1,
            .pBufferInfo = &voxel_chunk_indices,
            .pImageInfo = NULL,
            .pTexelBufferView = NULL,
        };

        VkDescriptorBufferInfo voxel_colors = {
            .buffer = gpu_scene->voxel_color_buffer[frame_index].buffer,
            .offset = 0,
            .range = sizeof(world->voxel_colors),
        };

        const VkWriteDescriptorSet voxel_colors_write = {
            .sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
            .dstSet = descriptor_set,
            .dstBinding = 0,
            .dstArrayElement = 1,
            .descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,
            .descriptorCount = 1,
            .pBufferInfo = &voxel_colors,
            .pImageInfo = NULL,
            .pTexelBufferView = NULL,
        };

        const VkWriteDescriptorSet descriptor_writes[] = { voxel_chunks_write, voxel_chunks_indices_write, voxel_colors_write };

        fa_vulkan_backend_api->descriptor_write(FA_ARRAY_COUNT(descriptor_writes), descriptor_writes);
    }

    fa_vulkan_backend_api->submit_scene(&renderer->gpu_scene);
    FA_PROFILER_ZONE_END(ctx);
}

static void create(void)
{
    for (uint32_t i = 0; i < MAX_SWAPCHAIN_IMAGES; ++i)
    {
        renderer->gpu_scene.draw_data_dirty[i] = true;
    }

    fa_gpu_scene_t* gpu_scene = &renderer->gpu_scene;
    gpu_scene->draw_count = 1;

    for (uint32_t i = 0; i < MAX_SWAPCHAIN_IMAGES; ++i)
    {
        VkDrawIndexedIndirectCommand draw_cmd;
        draw_cmd.firstIndex = 0;
        draw_cmd.indexCount = 3;
        draw_cmd.vertexOffset = 0;
        draw_cmd.firstInstance = 0;
        draw_cmd.instanceCount = 1;

        const uint32_t frame_index = i;

        gpu_scene->command_draw_data_buffer[frame_index] = fa_vulkan_backend_api->buffer_alloc(sizeof(draw_cmd), VK_BUFFER_USAGE_INDIRECT_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, "CommandDrawData");
        fa_vulkan_backend_api->buffer_upload(gpu_scene->command_draw_data_buffer[frame_index], &draw_cmd, 0, sizeof(draw_cmd));

        const vec2s fullscreen_quad_verts[] = { { -1, -1 }, { 3, -1 }, { -1, 3 } };
        const uint32_t fullscreen_quad_indices[] = { 0, 1, 2 };

        gpu_scene->global_vertex_buffer[frame_index] = fa_vulkan_backend_api->buffer_alloc(sizeof(fullscreen_quad_verts), VK_BUFFER_USAGE_VERTEX_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, "GVertexBuffer");
        gpu_scene->global_index_buffer[frame_index] = fa_vulkan_backend_api->buffer_alloc(sizeof(fullscreen_quad_indices), VK_BUFFER_USAGE_INDEX_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, "GIndexBuffer");
        fa_vulkan_backend_api->buffer_upload(gpu_scene->global_vertex_buffer[frame_index], fullscreen_quad_verts, 0, sizeof(fullscreen_quad_verts));
        fa_vulkan_backend_api->buffer_upload(gpu_scene->global_index_buffer[frame_index], fullscreen_quad_indices, 0, sizeof(fullscreen_quad_indices));
    }
}

static void destroy(void)
{
    for (uint32_t i = 0; i < MAX_SWAPCHAIN_IMAGES; ++i)
    {
        const fa_gpu_scene_t* gpu_scene = &renderer->gpu_scene;
        fa_vulkan_backend_api->buffer_free(gpu_scene->command_draw_data_buffer[i]);

        fa_vulkan_backend_api->buffer_free(gpu_scene->voxel_chunk_buffer[i]);
        fa_vulkan_backend_api->buffer_free(gpu_scene->voxel_chunk_indices[i]);
        fa_vulkan_backend_api->buffer_free(gpu_scene->voxel_color_buffer[i]);

        fa_vulkan_backend_api->buffer_free(gpu_scene->global_index_buffer[i]);
        fa_vulkan_backend_api->buffer_free(gpu_scene->global_vertex_buffer[i]);
    }
}

static struct fa_renderer_api renderer_api = {
    .create = create,
    .destroy = destroy,
    .frame_begin = frame_begin,
    .frame_draw = frame_draw,
};

FA_LIB_EXPORT void fa_load_module(struct fa_api_registry* registry, bool load)
{
    fa_logger_api = registry->get(FA_LOGGER_API_NAME);
    fa_allocator_api = registry->get(FA_ALLOCATOR_API_NAME);
    fa_vulkan_backend_api = registry->get(FA_VULKAN_BACKEND_API_NAME);
    fa_job_scheduler_api = registry->get(FA_JOB_API_NAME);
    fa_voxel_api = registry->get(FA_VOXEL_API_NAME);

    fa_add_or_remove_api(&renderer_api, FA_RENDERER_API_NAME, load);

    renderer = (renderer_t*)registry->static_variable(FA_RENDERER_API_NAME, "vulkan_backend", sizeof(renderer_t));
}
