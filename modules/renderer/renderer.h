#pragma once

#include <core/core_types.h>

struct fa_renderer_api
{
    void (*create)(void);
    void (*destroy)(void);

    void (*frame_begin)(void);
    void (*frame_draw)(void);
};

#define FA_RENDERER_API_NAME "FA_RENDERER_API"