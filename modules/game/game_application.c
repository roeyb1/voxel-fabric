#include <core/api_registry.h>
#include <core/application_api.h>
#include <core/core_defines.h>
#include <core/core_types.h>
#include <core/error.h>
#include <core/log.h>
#include <core/module_api.h>
#include <core/os.h>
#include <core/profiler.h>

#include <modules/ui/ui.h>
#include <modules/window/window_api.h>
#include <modules/world_gen/world_gen.h>

#define CIMGUI_DEFINE_ENUMS_AND_STRUCTS
#include <cglm/struct.h>
#include <vendor/cimgui/cimgui.h>
#include <vendor/iconfontheaders/IconsFontAwesome5.h>
#include <vulkan/vulkan.h>

#include <modules/asset_import/asset_import.h>
#include <modules/renderer/renderer.h>
#include <modules/ui/style_colors.inl>
#include <modules/ui_widgets/ui_widgets.h>
#include <modules/voxel/voxel_module.h>
#include <modules/vulkan_backend/vulkan_backend.h>

static struct fa_logger_api* fa_logger_api;
static struct fa_window_api* fa_window_api;
static struct fa_vulkan_backend_api* fa_vulkan_backend_api;
static struct fa_renderer_api* fa_renderer_api;
static struct fa_ui_api* fa_ui_api;
static struct fa_ui_widgets_api* fa_widgets_api;
static struct fa_os_api* fa_os_api;
static struct fa_asset_import_api* fa_asset_import_api;
static struct fa_voxel_api* fa_voxel_api;
static struct fa_world_gen_api* fa_world_gen_api;

// Uncomment this line to enable conservative viewport resizing (only resizes the framebuffer when
// the manual resize operation finished
#define DISABLE_CONSERVATIVE_RESIZE 0

typedef struct fa_application_t
{
    // #todo: multiple windows
    /** Store a handle to the main game window */
    fa_window_t window_handle;

    fa_render_backend_t* render_backend;

    fa_clock_t clock;
    double time;
    double delta_time;
} fa_application_t;

static fa_application_t* game;

static void draw_dockspace(void)
{
    FA_PROFILER_ZONE(ctx, true);
    static ImGuiDockNodeFlags DockspaceFlags = ImGuiDockNodeFlags_None;

    // We are using the ImGuiWindowFlags_NoDocking flag to make the parent window not dockable into,
    // because it would be confusing to have two docking targets within each others.
    ImGuiWindowFlags WindowFlags = ImGuiWindowFlags_NoDocking;
    const ImGuiViewport* ImGuiMainViewport = igGetMainViewport();
    igSetNextWindowPos(ImGuiMainViewport->Pos, 0, (ImVec2){ 0, 0 });
    igSetNextWindowSize(ImGuiMainViewport->Size, 0);
    igSetNextWindowViewport(ImGuiMainViewport->ID);
    igPushStyleVar_Float(ImGuiStyleVar_WindowRounding, 0.0f);
    igPushStyleVar_Float(ImGuiStyleVar_WindowBorderSize, 0.0f);
    igPushStyleColor_Vec4(ImGuiCol_MenuBarBg, (ImVec4){ 0.14f, 0.14f, 0.14f, 1.00f });
    WindowFlags |= ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoMove;
    WindowFlags |= ImGuiWindowFlags_NoBringToFrontOnFocus | ImGuiWindowFlags_NoNavFocus;

    // When using ImGuiDockNodeFlags_PassthruCentralNode, DockSpace() will render our background and handle the pass-thru hole, so we ask Begin() to not render a background.
    if (DockspaceFlags & ImGuiDockNodeFlags_PassthruCentralNode)
    {
        WindowFlags |= ImGuiWindowFlags_NoBackground;
    }

    // Important: note that we proceed even if Begin() returns false (aka window is collapsed).
    // This is because we want to keep our DockSpace() active. If a DockSpace() is inactive,
    // all active windows docked into it will lose their parent and become undocked.
    // We cannot preserve the docking relationship between an active window and an inactive docking, otherwise
    // any change of dockspace/settings would lead to windows being stuck in limbo and never being visible.
    igPushStyleVar_Vec2(ImGuiStyleVar_WindowPadding, (ImVec2){ 0.0f, 0.0f });
    igBegin("DockSpace", NULL, WindowFlags);

    igPopStyleVar(1);
    igPopStyleColor(1);
    igPopStyleVar(2);

    const ImGuiID DockspaceID = igGetID_Str("DockSpace");
    igDockSpace(DockspaceID, (ImVec2){ 0.0f, 0.0f }, DockspaceFlags, NULL);

    igEnd();
    FA_PROFILER_ZONE_END(ctx);
}

static bool scene_color_needs_resize = false;
static vec2s scene_framebuffer_size = { { 1280, 720 } };

static void draw_scene_viewport(void)
{
    FA_PROFILER_ZONE(ctx, true);
    const uint64_t scene_color_descriptor = fa_vulkan_backend_api->get_scene_color_descriptor();

    igBegin(ICON_FA_HASHTAG " Scene", NULL, ImGuiWindowFlags_NoTitleBar);

    ImVec2 current_scene_panel_size;
    igGetContentRegionAvail(&current_scene_panel_size);

    // Resize when our framebuffer size is not equal to the current framebuffer size.
    // Wait until the mouse is released before triggered the resize so we don't end up
    // resizing every frame along the way.
    if ((current_scene_panel_size.x > 0 && current_scene_panel_size.y > 0) &&
        (scene_framebuffer_size.x != current_scene_panel_size.x ||
         scene_framebuffer_size.y != current_scene_panel_size.y))
    {
        if (FA_IS_DEFINED(DISABLE_CONSERVATIVE_RESIZE) || !igIsMouseDown(ImGuiMouseButton_Left))
        {
            scene_color_needs_resize = true;
            scene_framebuffer_size.x = current_scene_panel_size.x;
            scene_framebuffer_size.y = current_scene_panel_size.y;
        }
    }

    ImVec2 content_region;
    igGetContentRegionAvail(&content_region);

    igImage((ImTextureID)scene_color_descriptor,
            current_scene_panel_size,
            (ImVec2){ 0.f, 0.f },
            (ImVec2){ 1.f, 1.f },
            (ImVec4){ 1.f, 1.f, 1.f, 1.f },
            (ImVec4){ 0.f, 0.f, 0.f, 0.f });

    igEnd();
    FA_PROFILER_ZONE_END(ctx);
}

#pragma region Application Api Implementation

static bool game_tick(fa_application_t* app)
{
    const fa_window_t window = app->window_handle;

    // Update frame time and delta time
    const fa_clock_t now = fa_os_api->time->now();
    const double delta_time = fa_os_api->time->time_sub(now, app->clock);

    app->clock = now;
    app->delta_time = delta_time;
    app->time += delta_time;

    fa_window_api->update_window(window);
    fa_renderer_api->frame_begin();

    draw_dockspace();

    draw_scene_viewport();

    if (igBegin("Stats", NULL, 0))
    {
        igText("Frame time: %.3f ms", 1000 * delta_time);
        igEnd();
    }

    fa_renderer_api->frame_draw();

    if (scene_color_needs_resize)
    {
        fa_vulkan_backend_api->resize_scene_color_framebuffer((uint32_t)scene_framebuffer_size.x, (uint32_t)scene_framebuffer_size.y);
        scene_color_needs_resize = false;
    }

    return !fa_window_api->window_wants_close(window);
}

static void game_destroy(void)
{
}

static fa_application_t* game_create(void)
{
    fa_voxel_api->create_world();

    // create a random 2d grid of voxels for testing
    fa_world_gen_api->set_world_seed(1234568910);
    const int32_t grid_width = 192;
    for (int32_t x = -grid_width + 1; x < grid_width + 1; ++x)
    {
        for (int32_t z = -grid_width + 1; z < grid_width + 1; ++z)
        {
            const int32_t world_height = (int32_t)(fa_world_gen_api->compute_world_heightmap(x, z) * 128) - 64;

            ivec3s pos = (ivec3s){ x, 0, z };
            vec3s color = GLMS_VEC3_ZERO;

            // dirt pass
            for (int32_t y = -7; y < world_height - 1; ++y)
            {
                pos.y = y;
                color = (vec3s){ 0.4f, 0.4f, 0.4f };
                fa_voxel_api->voxel_add(pos, color);
            }
            for (int32_t y = world_height - 1; y < world_height; ++y)
            {
                pos.y = y;
                color = (vec3s){ 0.588f, 0.294f, 0.f };
                fa_voxel_api->voxel_add(pos, color);
            }

            // grass pass
            pos.y = world_height;
            color = (vec3s){ 0.f, 0.5f, 0.f };
            if (world_height > 20)
            {
                if (world_height > 30 || rand() / (double)RAND_MAX < (world_height - 20) / 10.f)
                    color = GLMS_VEC3_ONE;
            }
            fa_voxel_api->voxel_add(pos, color);

            // ocean pass
            for (int32_t y = world_height; y <= -2; ++y)
            {
                pos.y = y;
                color = (vec3s){ 0.f, 0.4784f, 0.6471f };
                fa_voxel_api->voxel_add(pos, color);
            }
        }
    }

    game->window_handle = fa_window_api->get_window();
    return (fa_application_t*)&game;
}

static fa_application_t* game_get(void)
{
    return (fa_application_t*)game;
}

static void game_exit(fa_application_t* app)
{
    fa_ui_api->destroy();

    fa_window_api->close_window(app->window_handle);
}

#pragma endregion

#pragma region Module

static fa_application_api game_application_api = {
    .create = game_create,
    .get = game_get,
    .destroy = game_destroy,
    .exit = game_exit,
    .tick = game_tick,
};

FA_LIB_EXPORT void fa_load_module(struct fa_api_registry* registry, bool load)
{
    fa_logger_api = registry->get(FA_LOGGER_API_NAME);
    fa_window_api = registry->get(FA_WINDOW_API_NAME);
    fa_vulkan_backend_api = registry->get(FA_VULKAN_BACKEND_API_NAME);
    fa_renderer_api = registry->get(FA_RENDERER_API_NAME);
    fa_ui_api = registry->get(FA_UI_API_NAME);
    fa_widgets_api = registry->get(FA_UI_WIDGETS_API_NAME);
    fa_os_api = registry->get(FA_OS_API_NAME);
    fa_asset_import_api = registry->get(FA_ASSET_IMPORT_API_NAME);
    fa_voxel_api = registry->get(FA_VOXEL_API_NAME);
    fa_world_gen_api = registry->get(FA_WORLD_GEN_API);

    fa_add_or_remove_api(&game_application_api, FA_APPLICATION_API_NAME, load);

    game = registry->static_variable(FA_APPLICATION_API_NAME, "game", sizeof(fa_application_t));
}

#pragma endregion