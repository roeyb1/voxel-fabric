#pragma once

struct fa_world_gen_api
{
    void (*set_world_seed)(uint64_t seed);
    double (*compute_world_heightmap)(double x, double z);
};

#define FA_WORLD_GEN_API "FA_WORLD_GEN_API"