#include <cglm/struct.h>

#include "world_gen.h"

#include <core/api_registry.h>
#include <core/core_defines.h>

#include "perlin_noise.h"

typedef struct world_gen_settings_t
{
    uint64_t seed;
} world_gen_settings_t;

static world_gen_settings_t* world_gen_settings;

static double compute_world_height(double x, double z)
{
    return fa_perlin_2d(x, z, (int)world_gen_settings->seed);
}

static void set_world_seed(uint64_t seed)
{
    world_gen_settings->seed = seed;
}

struct fa_world_gen_api world_gen_api = {
    .compute_world_heightmap = compute_world_height,
    .set_world_seed = set_world_seed
};

FA_LIB_EXPORT void fa_load_module(struct fa_api_registry* registry, bool load)
{
    fa_add_or_remove_api(&world_gen_api, FA_WORLD_GEN_API, load);

    world_gen_settings = (world_gen_settings_t*)registry->static_variable(FA_WORLD_GEN_API, "world_gen_settings", sizeof(world_gen_settings_t));
}