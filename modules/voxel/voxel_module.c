#include <cglm/struct.h>

#include "voxel_module.h"

#include <string.h>

#include <core/allocator.h>
#include <core/api_registry.h>
#include <core/array.h>
#include <core/core_defines.h>
#include <core/error.h>
#include <core/job_scheduler.h>
#include <core/log.h>
#include <core/module_api.h>
#include <core/profiler.h>
#include <core/stretchy_buffers.inl>

#include <modules/renderer/renderer.h>

struct fa_logger_api* fa_logger_api;
struct fa_allocator_api* fa_allocator_api;
struct fa_job_scheduler_api* fa_job_scheduler_api;
struct fa_renderer_api* fa_renderer_api;

#define LOG_CATEGORY "Voxel"

static fa_voxel_world_t* world;

static inline size_t get_3d_index(ivec3s pos, ivec3s size)
{
    return pos.x + size.x * pos.y + size.x * size.y * pos.z;
}

static void create_world(void)
{
    memset(world->chunk_data, 0, sizeof(world->chunk_data));
    fa_sbuff_set_capacity(world->free_chunk_indices, FA_ARRAY_COUNT(world->chunk_indices));
    for (int32_t i = FA_ARRAY_COUNT(world->chunk_indices) - 1; i >= 0; --i)
    {
        world->chunk_indices[i] = UNLOADED_CHUNK_INDEX;
        fa_sbuff_push(world->free_chunk_indices, i);
    }
}

static ivec3s transform_to_voxel_space(ivec3s pos)
{
    ivec3s voxel_space = pos;
    voxel_space.x += FA_VOXEL_WORLD_WIDTH_IN_CHUNKS * FA_VOXEL_CHUNK_WIDTH / 2 - 1;
    voxel_space.y += FA_VOXEL_WORLD_WIDTH_IN_CHUNKS * FA_VOXEL_CHUNK_WIDTH / 2 - 1;
    voxel_space.z += FA_VOXEL_WORLD_WIDTH_IN_CHUNKS * FA_VOXEL_CHUNK_WIDTH / 2 - 1;
    return voxel_space;
}

static void voxel_add(ivec3s pos, vec3s color)
{
    FA_PROFILER_ZONE(ctx, true);

    const ivec3s voxel_world_pos = transform_to_voxel_space(pos);

    ivec3s chunk_pos;
    chunk_pos.x = voxel_world_pos.x / FA_VOXEL_CHUNK_WIDTH;
    chunk_pos.y = voxel_world_pos.y / FA_VOXEL_CHUNK_WIDTH;
    chunk_pos.z = voxel_world_pos.z / FA_VOXEL_CHUNK_WIDTH;

    const size_t chunk_id = get_3d_index(chunk_pos, FA_VOXEL_WORLD_SIZE);
    int32_t chunk_index = world->chunk_indices[chunk_id];
    // if the chunk the voxel is being added to is not loaded, load it now
    if (chunk_index == UNLOADED_CHUNK_INDEX)
    {
        // #todo [roey]: this needs to be dealt with properly
        check(fa_sbuff_num(world->free_chunk_indices) > 0);
        chunk_index = fa_sbuff_pop(world->free_chunk_indices);
        world->chunk_indices[chunk_id] = chunk_index;
    }

    // position of the voxel within a chunk
    ivec3s local_voxel_pos;
    local_voxel_pos.x = voxel_world_pos.x % FA_VOXEL_CHUNK_WIDTH;
    local_voxel_pos.y = voxel_world_pos.y % FA_VOXEL_CHUNK_WIDTH;
    local_voxel_pos.z = voxel_world_pos.z % FA_VOXEL_CHUNK_WIDTH;

    const size_t local_voxel_3d_index = get_3d_index(local_voxel_pos, FA_VOXEL_CHUNK_SIZE);
    const size_t voxel_word_index = local_voxel_3d_index / 32;
    const size_t voxel_bit_index = local_voxel_3d_index % 32;

    FA_SET_BIT(world->chunk_data[chunk_index * fa_cube(FA_VOXEL_CHUNK_WIDTH) / 32 + voxel_word_index], voxel_bit_index);

    world->voxel_colors[chunk_index * fa_cube(FA_VOXEL_CHUNK_WIDTH) + local_voxel_3d_index] = (vec4s){ color.x, color.y, color.z, 1.f };

    FA_PROFILER_ZONE_END(ctx);
}

static fa_voxel_world_t* get_world(void)
{
    return world;
}

static struct fa_voxel_api voxel_api = {
    .create_world = create_world,
    .voxel_add = voxel_add,
    .get_world = get_world,
};

FA_LIB_EXPORT void fa_load_module(struct fa_api_registry* registry, bool load)
{
    fa_logger_api = registry->get(FA_LOGGER_API_NAME);
    fa_allocator_api = registry->get(FA_ALLOCATOR_API_NAME);
    fa_job_scheduler_api = registry->get(FA_JOB_API_NAME);
    fa_renderer_api = registry->get(FA_RENDERER_API_NAME);

    fa_add_or_remove_api(&voxel_api, FA_VOXEL_API_NAME, load);

    world = (fa_voxel_world_t*)registry->static_variable(FA_VOXEL_API_NAME, "voxel_world", sizeof(fa_voxel_world_t));
}