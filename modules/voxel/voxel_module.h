#pragma once

#include <core/core_defines.h>
#include <core/core_types.h>

#define FA_VOXEL_CHUNK_WIDTH 8

#define FA_VOXEL_CHUNK_SIZE \
    (ivec3s) { FA_VOXEL_CHUNK_WIDTH, FA_VOXEL_CHUNK_WIDTH, FA_VOXEL_CHUNK_WIDTH }

#define FA_VOXEL_WORLD_WIDTH_IN_CHUNKS 48
#define FA_VOXEL_WORLD_SIZE \
    (ivec3s) { FA_VOXEL_WORLD_WIDTH_IN_CHUNKS, FA_VOXEL_WORLD_WIDTH_IN_CHUNKS, FA_VOXEL_WORLD_WIDTH_IN_CHUNKS }

#define UNLOADED_CHUNK_INDEX -1

/**
 * World is composed of chunks which is currently limited to 64x64x64 voxels
 * #todo [roey]: streaming can allow the world to become infinite.
 */
typedef struct fa_voxel_world_t
{
    FA_ALIGN(4)
    int32_t chunk_indices[fa_cube(FA_VOXEL_WORLD_WIDTH_IN_CHUNKS)];

    FA_ALIGN(4)
    uint32_t chunk_data[fa_cube(FA_VOXEL_CHUNK_WIDTH) * fa_cube(FA_VOXEL_WORLD_WIDTH_IN_CHUNKS) / 32];

    FA_ALIGN(4)
    vec4s voxel_colors[fa_cube(FA_VOXEL_CHUNK_WIDTH) * fa_cube(FA_VOXEL_WORLD_WIDTH_IN_CHUNKS)];
    /* sbuff */ uint32_t* free_chunk_indices;
} fa_voxel_world_t;

struct fa_voxel_api
{
    void (*create_world)(void);
    fa_voxel_world_t* (*get_world)(void);

    void (*voxel_add)(ivec3s pos, vec3s color);
    void (*voxel_remove)(ivec3s pos);
};

#define FA_VOXEL_API_NAME "FA_VOXEL_API"