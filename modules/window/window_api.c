#include "window_api.h"
#include <core/api_registry.h>
#include <core/atomic.h>
#include <core/core_defines.h>
#include <core/error.h>
#include <core/log.h>
#include <core/profiler.h>

#include <GLFW/glfw3.h>
#define STB_IMAGE_IMPLEMENTATION
#include <vendor/stb/stb_image.h>

#define LOG_CATEGORY "Window"

static struct fa_logger_api* fa_logger_api = NULL;

static fa_window_t* window_handle;

static float dpi_scale_factor = 1.f;

static void glfw_error_callback(int code, const char* description)
{
    FA_LOG_CATEGORY(Error, "GLFW error (%d); %s", code, description);
}

static void init_glfw(void)
{
    if (!glfwInit())
    {
        FA_LOG_CATEGORY(Fatal, "Failed to initialize glfw!");
    }
    checkf(glfwVulkanSupported(), "Vulkan not supported!");

    glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);

    GLFWmonitor* monitor = glfwGetPrimaryMonitor();
    float xscale, yscale;
    glfwGetMonitorContentScale(monitor, &xscale, &yscale);
    if (xscale > 1 || yscale > 1)
    {
        dpi_scale_factor = xscale;
        glfwWindowHint(GLFW_SCALE_TO_MONITOR, GLFW_TRUE);
    }

    glfwSetErrorCallback(glfw_error_callback);

    FA_LOG_CATEGORY(Info, "Initialized");
}

static fa_window_t create_window(uint32_t width, uint32_t height, const char* window_title)
{
    GLFWwindow* handle = glfwCreateWindow(width, height, window_title, NULL, NULL);

    *window_handle = (fa_window_t){ .handle = (uint64_t)handle };

    return *window_handle;
}

static void set_window_icon(fa_window_t window, const char* image_file)
{
    // setup icon
    int width, height, channels;
    stbi_uc* pixel_data = stbi_load(image_file, &width, &height, &channels, STBI_rgb_alpha);
    GLFWimage image = {
        .height = height,
        .width = width,
        .pixels = pixel_data,
    };
    glfwSetWindowIcon((GLFWwindow*)window.handle, 1, &image);

    stbi_image_free(pixel_data);
}

static fa_window_t get_window(void)
{
    return *window_handle;
}

static void destroy_window(fa_window_t window)
{
    glfwDestroyWindow((GLFWwindow*)window.handle);
}

static void update_window(fa_window_t window)
{
    FA_PROFILER_ZONE(ctx, true);
    glfwPollEvents();
    FA_PROFILER_ZONE_END(ctx);
}

static bool window_wants_close(fa_window_t window)
{
    return glfwWindowShouldClose((GLFWwindow*)window.handle);
}

static void close_window(fa_window_t window)
{
    glfwSetWindowShouldClose((GLFWwindow*)window.handle, true);
}

static float get_dpi_scale_factor(fa_window_t window)
{
    return dpi_scale_factor;
}

static void shutdown_glfw(void)
{
    glfwDestroyWindow((GLFWwindow*)window_handle->handle);
    glfwTerminate();
}

struct fa_window_api fa_window_api = {
    .init = init_glfw,
    .shutdown = shutdown_glfw,
    .create_window = create_window,
    .set_window_icon = set_window_icon,
    .get_window = get_window,
    .destroy_window = destroy_window,
    .update_window = update_window,
    .window_wants_close = window_wants_close,
    .close_window = close_window,
    .get_dpi_scale_factor = get_dpi_scale_factor,
};

FA_LIB_EXPORT void fa_load_module(struct fa_api_registry* registry, bool load)
{
    fa_logger_api = registry->get(FA_LOGGER_API_NAME);

    fa_add_or_remove_api(&fa_window_api, FA_WINDOW_API_NAME, load);

    window_handle = registry->static_variable(FA_WINDOW_API_NAME, "window_handle", sizeof(fa_window_t));
}