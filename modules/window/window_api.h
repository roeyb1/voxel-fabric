#pragma once

#include <core/core_types.h>

typedef struct fa_window_t
{
    uint64_t handle;
} fa_window_t;

struct fa_window_api
{
    void (*init)(void);

    void (*shutdown)(void);

    /** Create a new window. Returns a platform agnostic handle to that window. */
    fa_window_t (*create_window)(uint32_t width, uint32_t height, const char* window_title);

    /** Set the window's icon. */
    void (*set_window_icon)(fa_window_t window, const char* image_file);

    /** Get the main application window. Currently we don't really support multi-window. */
    fa_window_t (*get_window)(void);

    /** Destroy a window. */
    void (*destroy_window)(fa_window_t window);

    /** Updates the event loop of the window. */
    void (*update_window)(fa_window_t window);

    /** Queries if the window wants to be closed. */
    bool (*window_wants_close)(fa_window_t window);

    /** Signal to the window that it should close on the next update. */
    void (*close_window)(fa_window_t window);

    /** Returns the dpi scale factor for a given window. */
    float (*get_dpi_scale_factor)(fa_window_t window);
};

#define FA_WINDOW_API_NAME "FA_WINDOW_API"