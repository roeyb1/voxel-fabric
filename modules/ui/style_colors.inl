#pragma once

const ImVec4 primary = { 0.007f, 0.533f, 0.819f, 1.f };
const ImVec4 primary_light = { 0.368f, 0.721f, 1.f, 1.f };
const ImVec4 primary_dark = { 0.f, 0.356f, 0.623f, 1.f };

const ImVec4 surface_color = { 0.059f, 0.059f, 0.059f, 1.f };
const ImVec4 overlay_1 = { 0.07f, 0.07f, 0.07f, 1.f };
const ImVec4 overlay_2 = { 0.117f, 0.117f, 0.117f, 1.f };
const ImVec4 overlay_3 = { 0.133f, 0.133f, 0.133f, 1.f };
const ImVec4 overlay_4 = { 0.177f, 0.177f, 0.177f, 1.f };
const ImVec4 active = { 0.3f, 0.3f, 0.3f, 1.f };
